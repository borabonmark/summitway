@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1 || Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif


@extends('layouts.master-layout')


@section('content')
    
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Order</li>
      </ol>
        <div class="col-lg-12">        
            <div class="card mb-3">
              <div class="card-header">
                <i class="fa fa-bar-chart"></i> Details price
                    <span class="badge badge-warning pull-right"><h3>Status: {{$order->status}} </h3> </span>

            
                </div>
              <div class="card-body">
                   @if($order->status=='delivering' || $order->status=='delivered')
                    @else
                    <input type="hidden" name="order_id" value="{{$id}}" id="orderId">  
                    <label>Quantity</label>
                    <input type="number" class="form-control" id="qty" placeholder="Quantity" value="1">
                    <label>Enter barcode</label>
                    <input type="text" class="form-control" id="barCode" placeholder="Barcode">             
                    <button style="display: none;" id="newItem">Add</button>
                    @endif
                    <br>
                    @unless($order->status=='delivered')
                      @unless($order->status=='delivering')   
                        <button type="button" class="btn btn-primary btn-md float-right" data-toggle="modal" data-target="#insertModal">Add item by name search</button>
                      @else
                      @if(Auth::user()->store_id != 1)
                        <button type="button" class="btn btn-success btn-md float-right" data-toggle="modal" data-target="#deliverModal">Confirm delivery</button>
                        @endif
                      @endunless
                    @endunless
                    <button class="btn btn-success" type="submit" onclick="printJS({ printable: 'delivery', type: 'html', header: 'Delivery for {{ Auth::user()->name}}' })"><i class="fa fa-print"></i> Print</button> 

                      <!--id="newCustomer" data-toggle="modal" data-target="#inser1tModal onclick="printJS({ printable: 'items', type: 'html', header: 'Delivery for {{ Auth::user()->name}}' })""--> 
              </div>
              <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>                        
        </div>
            
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Order product
      </div>
        <div class="card-body"  >
          <div class="table-responsive">
            <table class="table table-bordered" id="delivery" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>QTY</th>
                  <th>Barcode</th>
                  <th>Description</th>
                  <th>Total</th>                  
                  <th>Price</th>
                  <th></th>
                  <th></th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($orderproducts as $product)
                    <tr>
                        <td>{{$product->qty}}</td>
                        <td>{{$product['itemlist']['barcode']}}</td>
                        <td>{{$product['itemlist']['desc']}}</td>
                        <td>P{{$product->price*$product->qty}}</td>
                        <td>P{{$product->price}}<td>
                        
                       
                        <td></td>
                        <td>
                        @unless($order->status=='delivering'||$order->status=='delivered' || Auth::user()->store_id!=1)
                          <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{$product->id}}">Remove</button>

                          <!-- Edit -->
                          <div class="modal fade" id="deleteModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              
                              <form role="form" method="post" action="{{action('OrderProductController@destroy', $product->id)}}">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalCenterTitle">Delete</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">

                                      {{csrf_field()}}
                                      <h3>
                                      <center>
                                      <input type="hidden" name="order_id" value="{{$product->order_id}}">
                                      <input type="hidden" name="item_id" value="{{$product->itemlist_id}}">
                                      <input type="hidden" name="qty" value="{{$product->qty}}">
                                      <input type="hidden" name="price" value="{{$product->price}}">
                                      Are you sure?
                                      </a>
                                      </h3>
                                      </center>
                                      <br>

                         
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  @method('DELETE')
                                  <button class="btn btn-danger" type="submit">Confirm</button>   
                                </div>
                              </form>
                              </div>
                            </div>
                          </div>
                         @endunless
                        </td> 
                    </tr> 
              
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>


          <!-- Insert -->
          <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
              
              <form role="form" method="post" action="{{url('orderproduct')}}">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">New data</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">      

                      @csrf
                      <label>Item name</label>
                      <br>
                      <select class="selectpicker" data-live-search="true" name="item_id">
                        @foreach($itemlist as $item)
                        <option data-tokens="{{$item->name}}" value="{{$item->barcode}}">{{$item->name}}, Php {{$item->price}}/PC</option>
                        @endforeach
                      </select>

                      <br>
                      <label>QTY</label>
                      <input class="form-control" id="qty" placeholder="QTY" name="qty" type="number" required="required">
                      <label>Barcode</label>                      
                      <input type="hidden" name="order_id" value="{{$id}}">

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button class="btn btn-danger" type="submit">Add</button>   
                </div>
              </form>

              </div>
            </div>
          </div>

          <!-- Confirm delivery -->
          <div class="modal fade" id="deliverModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
              
              <form role="form" method="post" action="{{action('OrderProductController@update', $id)}}">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                      @csrf
                      <label>Please confirm if delivery has been delivered</label>
                                                
                      <input type="hidden" name="order_id" value="{{$id}}">
                      <textarea class="form-control" placeholder="Notes: " name="notes"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  @method('PATCH')
                  <button class="btn btn-success" type="submit">Confirm delivery</button>   
                </div>
              </form>
              </div>
            </div>
          </div>
 
  @push('script')
     <script type="text/javascript">
          $(document).ready(function() {
            $('#delivery').DataTable( {
                  dom: 'Bfrtip',
                  buttons: [
                      {
                          extend: 'excelHtml5',
                          title: 'Delivery Details',
                          message: '',
                          message: '   ',
                          exportOptions: { columns: [ 0, 1, 2, 4, 5, 6 ] }
                      },
                      {
                          extend: 'pdfHtml5',
                           title: 'Delivery Details',
                           message: '',
                          exportOptions: { columns: [ 0, 1, 2, 4, 5, 6 ] }
                      }
                  ]
              } );

            $('#barCode').focus();
  
            $("#newItem").click(function(event){
              var order_id = $('#orderId').val();
              var barcode = $('#barCode').val();
              var qty = $('#qty').val();
              var csrf = $('input[name="_token"]').val(); 
              var id = $('input[name="customer_id"]').val();



                $.post('/orderproduct', {'barcode': barcode, 'qty': qty, '_token': csrf, 'customer_id': id, 'order_id': order_id}, function(data){
                  console.log(data);  

                  if(data == 'error')
                  {
                    alert('Product not exist!');
                  }

                  $('#currentItem').val(data);
                  
                  if (!isNaN(data)) {
                    var totalAmount = +$('#totalAmount').val()+ +(data*qty);
                    $('#totalAmount').val(totalAmount.toFixed(2));
                  }
                                
                  location.reload();
                });         

            });

            $("#barCode").keyup(function(event) {
                if (event.keyCode === 13) {
                    $("#newItem").click();
                    $('#barCode').val('');
                    $('#qty').val('1');
                    $('#barCode').focus();
                }
            });
        });
      </script>
            
  @endpush

@endsection 
