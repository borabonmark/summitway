@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1|| Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif


@extends('layouts.master-layout')


@section('content')
    
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Filter By Barcode</li>
      </ol>
        @if(session('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{session('error')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
        @endif


      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
           <i class="fa fa-table"></i> Filter By Barcode
           <br>
           <br>
            <form action="{{ route('inventories.store') }}" method="post">
                @csrf
                @method('POST')
                <div class="row">
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="barcode">
                    </div>
                    
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary form-control">Filter</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                   <p> Barcode: 
                       @if (!empty($product))
                        <strong>{{ $product->barcode }}</strong> -  {{ $product->desc}}
                       @endif
                    </p>
                   <br>
                </div>
            </div>
          <div class="table-responsive">
            <table class="table table-bordered" id="inventoryList" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Store</th>
                  <th>QTY</th>
                </tr>
              </thead>
              <tbody>
                @if (!empty($inventories))
                    @foreach($inventories as $item)
                        <tr>
                            <td>
                                {{ $item->store->name }}
                            </td> 
                            <td>
                                {{ $item->qty }}
                            </td> 
                        </tr>
                    @endforeach
                @endif
                
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>

@endsection