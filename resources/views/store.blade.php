@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1)
    <script>window.location = "dashboard";</script>
  @endunless
@endif

@extends('layouts.master-layout')
<style type="text/css">#store_1{display: none !important;}</style>

@section('content')
    
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Stores</li>
      </ol>
            
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Store List
          @if(Auth::user()->type===1)
          <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#insertModal">Add new</button>
          @endif
      </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered display" id="dataTable" style="width:100%">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Target&nbsp;Quota</th>
                  <th>Reach&nbsp;Quota</th>
                  <th>Date&nbsp;Created</th>
                  <th>Inventory</th>
                  @if(Auth::user()->type===1)
                  <th>Request</th>
                  <th>Action</th>
                  @endif
                </tr>
              </thead>
              <tbody>
                
                @foreach($stores as $store)
                      <tr id="store_{{$store['id']}}">
                          <td>{{$store['id']}}</td>

                          @if(Auth::user()->type===1)
                            <td><a href="{{ url('dashboard', ['store' => $store->id]) }}">{{$store['name']}}</a></td>
                          @else
                            <td>{{$store['name']}}</td>
                          @endif
                          <td>{{$store['desc']}}</td>
                          <td>{{$store['qouta']}}</td>
                          @if(Auth::user()->type===1)
                          <td><a href="{{ url('bonus', ['store' => $store->id]) }}">Check Qouta</a></td>
                          @endif
                          <td>{{$store['create_at']}}</td>
                          <td><a href="{{ url('outstock', ['store' => $store->id]) }}">Threshold</a></td>
                          @if(Auth::user()->type===1)
                          <td>
                           @foreach($store->reset as $reset)

                              @if($reset->status == true)
                                <button type="button" class="btn btn-info btn-sm " data-toggle="modal" data-target="#requestModal{{$store['id']}}"> <i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i> Request</button>

                              @endif
                            @endforeach
                          </td>

                          <td>
                            
                              <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editModal{{$store['id']}}">Edit</button>
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{$store['id']}}">Delete</button>

                               <button type="button" class="btn btn-warning btn-sm text-white" data-toggle="modal" data-target="#resetModal{{$store['id']}}">Reset</button>

                                <!-- Request -->
                                <div class="modal fade" id="requestModal{{$store['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                  
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                       @foreach($store->reset as $reset)
                                         @if($reset->status == true)

                                        <div class="modal-body">
                                              <p class="text-muted">
                                                 
                                                    {{ $reset->notes}}
                                              </p>
                                        </div>
                                        <form action="{{url('reset').'/'.$reset['id']}}" method="post">
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              @csrf
                                              @method('patch')
                                              <input type="hidden" name="store_id" value="{{$store['id']}}">
                                          <button class="btn btn-success" type="submit">Confirm</button>  
                                        </div>
                                        </form>  
                                        @endif
                                         @endforeach
                                    </div>
                                  </div>
                                </div>

                                <!-- Reset -->
                               <div class="modal fade" id="resetModal{{$store['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                          Are you sure?
                                          <br>
                                          <br>
                                          <small class="text-muted"><strong>Note:</strong> You have to upload new set of inventory data for this store so we can continue with the operation.</small>
                                    </div>
                                    <div class="modal-footer">
                                      <form action="{{url('import')}}" method="post"  enctype="multipart/form-data" style="display: inline;">
                                        @csrf
                                               <div class="row">
                                                 <div class="col-sm-6">
                                                   <input type="file" name="file" class="text-center">
                                                <input type="hidden" name="store_id" value="{{$store['id']}}">
                                                
                                                 </div>
                                                 <div class="col-sm-6">
                                                   <input type="submit" class="btn btn-danger text-center" value=" Upload & Reset" >
                                                 </div>
                                               </div>
                                      </form>  
                                    </div>
                                  </div>
                                </div>
                              </div>

                                <!-- delete -->
                                <div class="modal fade" id="deleteModal{{$store['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                    
                                    <form role="form" method="post" action="{{action('StoreController@destroy', $store['id'])}}">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                            @csrf
                                            <p class="alert alert-warning"><i class="fa fa-fw fa-exclamation-triangle"></i>You cannot reverse this action once taken</p>
                                            Are you sure?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger" type="submit">Delete</button>    
                                      </div>
                                    </form>

                                    </div>
                                  </div>
                                </div>

                                <!-- Edit -->
                                <div class="modal fade" id="editModal{{$store['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                    
                                    <form role="form" method="post" action="{{ route('store.update', $store->id)}}">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Update info</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                         
                                            {{csrf_field()}}
                                            <label for="">Store Name</label>
                                            <input class="form-control" placeholder="Branch Name" name="name" type="text" value="{{ $store->name }}">
                                            <label>Description</label>
                                            <input class="form-control" placeholder="Branch Details" name="desc" type="text" value="{{ $store->desc }}">
                                            <label>Qouta</label>
                                            <input class="form-control" placeholder="Quota" name="qouta" type="text" value="{{ $store->qouta }}">
                                            <label class="mt-3">Closing Time</label>
                                            {{-- <input class="form-control" placeholder="Closing Time" name="closing_time" type="text" value="{{ $store->closing_time }}"> --}}
                                            <select name="closing_time" class="form-control">
                                              <option value="{{ $store->closing_time }}" selected>{{ $store->closing_time }}</option>
                                              <option value="0:00">0:00</option>
                                              <option value="1:00">1:00</option>
                                              <option value="2:00">2:00</option>
                                              <option value="3:00">3:00</option><option value="4:00">4:00</option><option value="5:00">5:00</option><option value="6:00">6:00</option><option value="7:00">7:00</option><option value="8:00">8:00</option><option value="9:00">9:00</option><option value="10:00">10:00</option><option value="11:00">11:00</option><option value="12:00">12:00</option><option value="13:00">13:00</option><option value="14:00">14:00</option><option value="15:00">15:00</option><option value="16:00">16:00</option><option value="17:00">17:00</option><option value="18:00">18:00</option><option value="19:00">19:00</option><option value="20:00">20:00</option><option value="21:00">21:00</option><option value="22:00">22:00</option><option value="23:00">23:00</option><option value="24:00">24:00</option></select>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input name="_method" type="hidden" value="PATCH">
                                        <button class="btn btn-danger" type="submit">Update</button>   
                                      </div>
                                    </form>

                                    </div>
                                  </div>
                                </div>
                          </td> 
                          @endif
                      </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>


  <!-- Button trigger modal -->  

    <!-- Insert -->
    <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        
        <form role="form" method="post" action="{{url('store')}}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Create New Store</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             
                {{csrf_field()}}
                <input class="form-control" placeholder="Branch Name" name="name" type="text">
                <label></label>
                <input class="form-control" placeholder="Branch Details" name="desc" type="text">
                <label></label>
                <input class="form-control" placeholder="Qouta" name="qouta" type="number">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <input type="submit" name="submit" class="btn btn-primary">    
          </div>
        </form>

        </div>
      </div>
    </div>

@endsection
