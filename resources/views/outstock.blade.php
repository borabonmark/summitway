@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1 || Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif

@extends('layouts.master-layout')


@section('content')
    
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Out Stock</li>
      </ol>

        @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{session('success')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
            
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Out Stock for  <strong>{{ $storename[0]->name }} </strong>
          <span class="pull-right"> Stocks are Less Than 100pcs</span>
      </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Barcode</th>
                  <th>Details</th>
                  <th>Qty</th>
                </tr>
              </thead>
              <tbody>
                @foreach($items as $out)
                    <tr>
                        
                        <td>{{ $out->id}} </td>
                        <td>{{ $out->barcode}} </td>
                        <td>{{ $out->desc}} </td>
                        <td>{{ $out->qty}} </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>


  <!-- Button trigger modal -->  

    <!-- Insert -->
    <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        
        <form role="form" method="post" action="{{url('store')}}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Create New Store</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             
                {{csrf_field()}}
                <input class="form-control" placeholder="Item Name" name="name" type="text">
                <label></label>
                <input class="form-control" placeholder="Item description" name="desc" type="text">
                <label></label>
                <input class="form-control" placeholder="Qouta" name="qouta" type="number">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <input type="submit" name="submit" class="btn btn-primary">    
          </div>
        </form>

        </div>
      </div>
    </div>

@endsection