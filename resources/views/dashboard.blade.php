@extends('layouts.master-layout')


@section('content')
<div class="container-fluid">
  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
    @empty($store->name)
      <li class="breadcrumb-item active">Dashboard</li>
    @else
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
      <li class="breadcrumb-item active">{{ $store->name }}</li>
    @endisset
  </ol>

  
  @foreach($amountTotalFor as $index => $totalFor) 
    <!-- Line Chart -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-area-chart"></i> Total Amount {{ $totalFor }} Per Day
      </div>
      <div class="card-body">
        <canvas id="perDayAmountTotalChart{{$index}}" width="100%" height="30"></canvas>
      </div>
    </div>

    <!-- Bar Chart -->
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-bar-chart"></i> Total Amount {{ $totalFor }} Per Month
      </div>
      <div class="card-body">
        <canvas id="perMonthAmountTotalChart{{$index}}" width="100%" height="30"></canvas>
      </div>
    </div>
  @endforeach
  
</div>
@endsection

@push('script')
<script>
  //Line Chart: Amount Total Orders/Sales Per Day for current month
  var perDayAmountTotal = JSON.parse('{!! json_encode($perDayAmountTotal) !!}');
  $.each(perDayAmountTotal, function(index, value){
    var perDayDays = Object.values(value.days);
    var perDayTotal = Object.values(value.amount_total);

    var ctxPerDayAmountTotalChart = document.getElementById("perDayAmountTotalChart" + index);
    var perDayAmountTotalChart = new Chart(ctxPerDayAmountTotalChart, {
      type: 'line',
      data: {
        labels: perDayDays,
        datasets:[{
          label: "Total Amount",
          data: perDayTotal,
          lineTension: 0.3,
          backgroundColor: "rgba(2,117,216,0.2)",
          borderColor: "rgba(2,117,216,1)",
          pointRadius: 5,
          pointBackgroundColor: "rgba(2,117,216,1)",
          pointBorderColor: "rgba(255,255,255,0.8)",
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(2,117,216,1)",
          pointHitRadius: 20,
          pointBorderWidth: 2,
        }]
      }
    });
  });

  // Bar Chart: Amount Total Orders/Sales Per Month
  var perMonthAmountTotal = JSON.parse('{!! json_encode($perMonthAmountTotal) !!}');

  $.each(perMonthAmountTotal, function(index, value){
    var perMonthMonths = Object.values(value.months);
    var perMonthTotal = Object.values(value.amount_total);

    var ctxPerMonthAmountTotalChart = document.getElementById("perMonthAmountTotalChart" + index).getContext('2d');
    var perMonthAmountTotalChart = new Chart(ctxPerMonthAmountTotalChart, {
      type: 'bar',
      data: {
        labels: perMonthMonths,
        datasets:[{
          label: "Total Amount",
          data: perMonthTotal,
          backgroundColor: "rgba(2,117,216,1)",
          borderColor: "rgba(2,117,216,1)"
        }]
      }
    });
  });

</script>
@endpush