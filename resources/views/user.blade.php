@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1)
    <script>window.location = "dashboard";</script>
  @endunless
@endif


@extends('layouts.master-layout')


@section('content')
    
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Users</li>
      </ol>
     
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> User List
          <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#insertModal">Add new</button>
      </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Store</th>
                  <th>Type</th>                  
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Store</th>
                  <th>Type</th>                  
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>   
                        <td>{{$user->email}}</td>   
                        <td>{{$user->store->name}}</td>   
                        <td>
                          @if($user->type==1)
                            <span class="badge badge-pill badge-primary">Admin</span>
                          @else
                            <span class="badge badge-pill badge-secondary">Staff</span>
                          @endif
                        </td>
                        <td>
                          @if($user->type==1)
                          <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal{{$user->id}}">Edit</button>
                          <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{$user->id}}">Delete</button> 
                          @endif
                          <!-- Edit -->
                    <div class="modal fade" id="editModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                        
                        <form role="form" method="post" action="{{action('RegistrationController@update', $user->id)}}">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Update data</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                             
                                {{csrf_field()}}
                                  <label>Name</label>
                                  <input class="form-control" placeholder="Item Name" name="name" type="text" value="{{$user->name}}">
                                  <label>Email</label>
                                  <input class="form-control" placeholder="Item Name" name="email" type="email" value="{{$user->email}}">
                                  <label>Password</label>
                                  <input class="form-control" placeholder="Item Name" name="password" type="password" value="{{$user->password}}">
                                  @if(Auth::user()->store_id===1)
                                  <label>Store</label>
                                      <select class="form-control" name="store_id">
                                          @foreach($stores as $store)
                                            <option value="{{$store->id}}">{{$store->name}}</option>
                                          @endforeach
                                      </select>
                                  @else
                                          <input class="form-control" placeholder="Item Name" name="store_id" type="hidden" value="{{$user->store_id}}">
                                  @endif
                                  <label>Type</label>
                                      <select class="form-control" name="type">
                                        <option value="2">staff</option>
                                        <option value="1">admin</option>
                                      </select> 

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            @method('PATCH')
                            <button class="btn btn-danger" type="submit">Update</button>   
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>

                  <!-- delete -->
                    <div class="modal fade" id="deleteModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                        
                        <form role="form" method="post" action="{{action('RegistrationController@destroy', $user->id)}}">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                                @csrf
                                Are you sure?
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>    
                          </div>
                        </form>

                        </div>
                      </div>
                    </div> 

                        </td>             
                    </tr>       
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>

          <!-- Insert -->
          <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
              
              <form role="form" method="post" action="{{url('user')}}">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">New data</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                   
                      {{csrf_field()}}
                        <label>Name</label>
                        <input class="form-control" placeholder="User Name" name="name" type="text">
                        <label>Email</label>
                        <input class="form-control" placeholder="User Email" name="email" type="email">
                        <label>Password</label>
                        <input class="form-control" placeholder="Password" name="password" type="password">
                        @if(Auth::user()->store_id===1)
                        <label>Store</label>
                            <select class="form-control" name="store_id">
                                @foreach($stores as $store)
                                <option value="{{$store->id}}">{{$store->name}}</option>
                                @endforeach
                            </select>
                        @else
                                <input class="form-control" placeholder="Item Name" name="store_id" type="hidden" value="{{$user->store_id}}">
                        @endif
                        <label>Type</label>
                            <select class="form-control" name="type">
                              <option value="2">staff</option>
                              <option value="1">admin</option>
                            </select> 

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button class="btn btn-success" type="submit">Save</button>   
                </div>
              </form>

              </div>
            </div>
          </div>
@endsection