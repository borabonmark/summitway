@if(Auth::check())
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Summitway</title>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">

  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>

  <link href="https://printjs-4de6.kxcdn.com/print.min.css" rel="stylesheet">

  
  <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

<style type="text/css">
  #cashFund input{
    width: 65px;
    height: 25px;
    border: none;
  }
  
</style>
  @stack('styles')
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <div id="app">
    @include('layouts.side-nav')
    <div class="content-wrapper">
      <div class="mx-3">
        @include('components.flash')
      </div>
      @yield('content')

      <footer class="sticky-footer">
        <div class="container">
          <div class="text-center">
            <small>Copyright © Summitway 2018</small>
          </div>
        </div>
      </footer>

      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
      </a>

      <!-- Logout Modal-->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

              <a class="btn btn-primary" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
              
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <script src="{{asset('js/app.js')}}"></script>
  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <!-- Core plugin JavaScript-->
  <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
  <!-- Page level plugin JavaScript-->
  <script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
  <!-- Custom scripts for all pages-->
  <script src="{{asset('js/sb-admin.js')}}"></script>
  <!-- Custom scripts for this page-->
  {{-- <script src="{{asset('js/sb-admin-datatables.min.js')}}"></script> --}}
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
  <!-- Page level JavaScript-->


 
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>


<!-- CASH FUND SCRIPT-->
   <script src="{{asset('js/main.js')}}"></script>
  



  <!-- Notification -->
  <script>
    var notificationDropdown = $('#notification-dropdown');
    var newNotificationIndicator = $('#notification-indicator');
    var notificationsItems = $('#notifications-items');
    var userId = "{{ Auth::id() }}";

    notificationDropdown.trigger('click');

    notificationDropdown.on('click', function(){
      newNotificationIndicator.removeClass('d-block');
    });

    Echo.private('orderStatusUpdated.' + userId)
        .listen('OrderStatusUpdated', (data) => { 
        var orderLog = data.orderLog;
        var orderDate = new Date(orderLog.created_at);
        var existingNotificationItems = notificationsItems.html();
        var newNotificationItem = '<a class="dropdown-item" href="/orderproduct/' + orderLog.order_id + '">' + 
                                '<strong>' + orderLog.type.toUpperCase()  + '</strong>' +
                                '<span class="small float-right text-muted">' + orderDate.toLocaleTimeString('en-US', { hour12: true }) + '</span>' + 
                                '<div class="dropdown-message small">' + orderLog.message + '</div>' +
                              '</a>' + 
                              '<div class="dropdown-divider"></div>';

        newNotificationIndicator.addClass('d-block');
        notificationsItems.html(newNotificationItem + existingNotificationItems);
    });
  </script>
</body>
</html>

@else
<script>window.location = "login";</script>
@endif

@stack('script')
