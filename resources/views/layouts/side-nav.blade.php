        <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="#">
      @auth
        {{ Auth::user()->name }}
      @endauth
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        
        @if(Auth::user()->type===1)
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{url('/')}}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
        @endif
        @if(Auth::user()->store_id===1 && Auth::user()->type===1)
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('itemlist')}}">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Products</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('transactions.index')}}">
            <i class="fa fa-user-secret"></i>
            <span class="nav-link-text">Transactions Logs</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('inventories.index')}}">
            <i class="fa fa-fw fa-cubes"></i>
            <span class="nav-link-text">Check Stocks</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('pull-outs.index')}}">
            <i class="fa fa-fw fa-cubes"></i>
            <span class="nav-link-text">Pull Outs</span>
          </a>
        </li>
        @endif

        @if(Auth::user()->store_id===1 &&  Auth::user()->type===2)
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('itemlist')}}">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Products</span>
          </a>
        </li> 
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('order')}}">
            <i class="fa fa-fw fa-truck"></i>
            <span class="nav-link-text">Delivery</span>
          </a>  
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('store')}}">
            <i class="fa fa-fw fa-building"></i>
            <span class="nav-link-text">Store</span>
          </a>
        </li>  
        @endif

        @if(Auth::user()->store_id===1 && Auth::user()->type===1)
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('store')}}">
            <i class="fa fa-fw fa-building"></i>
            <span class="nav-link-text">Store</span>
          </a>
        </li>
        @endif

        @if(Auth::user()->type===1)

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('user')}}">
            <i class="fa fa-fw fa-users"></i>
            <span class="nav-link-text">User</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('order')}}">
            <i class="fa fa-fw fa-truck"></i>
            <span class="nav-link-text">Delivery</span>
          </a>  
        </li>
        
        
        @unless(Auth::user()->store_id===1)
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('inventory')}}">
            <i class="fa fa-fw fa-cubes"></i>
            <span class="nav-link-text">Inventory</span>
          </a>  
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{url('pos')}}">
            <i class="fa fa-fw fa-calculator"></i>
            <span class="nav-link-text">POS</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{url('return')}}">
            <i class="fa fa-fw fa-share-square"></i>
            <span class="nav-link-text">Return</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ url('bonus', ['store' => Auth::user()->store_id]) }}">
            <i class="fa fa-fw fa-usd"></i>
            <span class="nav-link-text">Bonus</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ url('expenses')}}">
            <i class="fa fa-fw fa-usd"></i>
            <span class="nav-link-text">Expenses</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('transactions.index')}}">
            <i class="fa fa-user-secret"></i>
            <span class="nav-link-text">Transactions Logs</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('pull-outs.index')}}">
            <i class="fa fa-fw fa-cubes"></i>
            <span class="nav-link-text">Pull Outs</span>
          </a>
        </li>
        @endunless
        @endif

        @if(Auth::user()->store_id !=1 && Auth::user()->type===2)
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('inventory')}}">
            <i class="fa fa-fw fa-cubes"></i>
            <span class="nav-link-text">Inventory</span>
          </a>  
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{url('pos')}}">
            <i class="fa fa-fw fa-calculator"></i>
            <span class="nav-link-text">POS</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{url('return')}}">
            <i class="fa fa-fw fa-share-square"></i>
            <span class="nav-link-text">Return</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="{{url('order')}}">
            <i class="fa fa-fw fa-truck"></i>
            <span class="nav-link-text">Delivery</span>
          </a>  
        </li>
        
        @endif
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item ">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="notification-dropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-bell"></i>
            <span class="d-lg-none">Notifications
              <span class="badge badge-pill badge-warning">6 New</span>
            </span>
            <span class="indicator text-warning d-none" id="notification-indicator">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="notification-dropdown">
            <h6 class="dropdown-header">Notifications:</h6>
            <div class="dropdown-divider"></div>
            <div id="notifications-items"></div>
          </div>
        </li>
        <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for...">
              <span class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>