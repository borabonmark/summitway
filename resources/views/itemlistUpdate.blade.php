@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1 || Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif

@extends('layouts.master-layout')


@section('content')
  <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Items</li>
      </ol>
      
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Product list
          <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#insertModal">Add new</button>
      </div>
        <div class="card-body">
          <form role="form" method="post" action="{{ route('itemlist.update' , $item['id'])}}">
                @csrf
                <label>Barcode</label>
                <input class="form-control" placeholder="Name" name="barcode" type="text" value="{{$item['barcode']}}" required="required">
                 <label>Name</label>
                <input class="form-control" placeholder="description" name="name" type="text" value="{{$item['name']}}" required="required">
                <label>Description</label>
                <input class="form-control" placeholder="description" name="desc" type="text" value="{{$item['desc']}}" required="required">
                <label>Category</label>
                <input class="form-control" placeholder="Category" name="category" type="text" value="{{$item['category']}}" required="required">
                <label>QTY</label>
                <input class="form-control" placeholder="Quantity" name="old_qty" type="number" value="{{$item['qty']}}" readonly>
                <label></label>
                <input class="form-control" placeholder="New Quantity" name="qty" type="number" required="required">
                <label>Price</label>
                <input class="form-control" placeholder="Item Price" name="price" type="number" value="{{$item['price']}}" step=".01" required="required">
                <hr>
                <div class="update-bonus">
                  <label class="radio-inline">
                  <input type="radio" name="bonus" value="1" {{ $item->bonus_type == 1 ? 'checked' : '' }}> 

                  Pin Money</label>
                  <label class="radio-inline"><input type="radio" name="bonus" value="2" {{ $item->bonus_type == 2 ? 'checked' : '' }}  > Equipment</label>

                  <input class="form-control  bonus_amount" placeholder="pin money amount" name="bonus_amount" type="text"  value="{{ $item->bonus_amount != null ? $item['bonus_amount'] : 0 }}" >
                </div>
              @method('PATCH')
              <hr>
              <button class="btn btn-primary" data-dismiss="modal">Submit</button>
          </form>
          </div>
        </div>
      </div>    
@endsection