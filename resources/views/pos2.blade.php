@extends('layouts.master-layout')


@section('content')
    
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">POS</li>
      </ol>

        @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session('success')}}
        
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif

        <form role="form" method="post" action="{{url('invoice')}}">                  
                @csrf            
            <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="New sale">    
        </form> 
        <hr>
        <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-money"></i> Point Of Sale  <span class="pull-right"> Total Bonus: <i class="fa fa-"></i> ₱ {{ $total_bonus }}</span> <span class="pull-right" style="margin-right: 20px;">  Grand Total: ₱ {{ $grand_total }} </span>
        </div>
        <div class="card-body">
          <div class="table-responsive"> 
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Total Amount</th>
                  <th>Bonus</th>
                  <th>Date/Time</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($sales as $sale)
                    <tr>
                        <td> ₱ {{ $sale->total }} </td>
                        <td>₱ {{ $sale->total_bonus }} </td>
                        <td>{{ $sale->created_at }}</td>                 
                        <td><a href="{{ route('pos.show',$sale->id) }}">Details</a></td>
                    </tr>              
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>

@endsection

