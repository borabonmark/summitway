

@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1)
    <script>window.location = "dashboard";</script>
  @endunless
@endif


@extends('layouts.master-layout')

@section('content')
 
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Bonus</li>
       
      </ol>
            
        <div class="card">
            <a href="{{url('/print').'/'.$storeId}}" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
        </div>
        <hr>
         <!-- Example DataTables Card-->
         <div class="card-header">
            <div class="row">
              <div class="col-sm-2">
                <i class="fa fa-table"></i> Today's Sales
            </div>
            <div class="col-sm-5">
              <form action="{{ url('export-daily-expenses') }}" method="POST">
                @csrf
                @method('POST')
                <input type="hidden" name="store_id" value="{{ $storeId }}" class="storeId">
                <div class="row"><div class="col-sm-7">
                  <input type="date" name="filter_date" required="required" class="form-control">
                </div>
                  <div class="col-sm-5"><button type="submit" class="form-control btn btn-danger">Export Expenses</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-sm-5">
              <form action="{{ url('export-daily-sales') }}" method="POST">
                @csrf
                @method('POST')
                <input type="hidden" name="store_id" value="{{ $storeId }}" class="storeId">
                <div class="row"><div class="col-sm-7">
                  <input type="date" name="filter_date" required="required" class="form-control">
                </div>
                  <div class="col-sm-5"><button type="submit" class="form-control btn btn-success">Export Daily Sales</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="card-body">
          <div class="table-responsive">

            <table class="table table-hover" id="dailysales" class="display" style="width:100%">
                  <thead>
                    <tr>
                      <th scope="col">Total&nbsp;Bonus</th>
                      <th>Barcode</th>
                      <th>Item Description</th>
                      <th>Price</th>
                      <th>Qty</th>
                      <th>Total</th>
                      <th>OS#</th>
                      <th>OR#</th>
                      <th>Transaction Time</th>
                    </tr>
                  </thead>
                  <tbody>                  
                  </tbody>
                </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>
    </div>
@endsection
@push('script')
 <script type="text/javascript">
      $(document).ready(function() {
        $('#dailysales').DataTable( {
                  ajax: window.location.origin+'/get-json/'+$('.storeId').val(),
                  columns: [
                      { data: 'bonus'},
                      { data: 'barcode'},
                      { data: 'description'},
                      { data: 'price'},
                      { data: 'qty'},
                      
                      { data: 'grandtotal'},
                      { data: 'OS#'},
                      { data: 'OR#'},
                      { data: 'created_at', render: function (data, type, row) {
                          return type === 'export' ?
                              data.replace( /[$,]/g, '' ) :
                              data;
                      } }
                  ],
                  dom: 'Bfrtip',
                  buttons: [
                      // {
                      //     extend: 'excelHtml5',
                      //     title: 'Daily Sales Report',
                      //     message: '',
                      //     exportOptions: { orthogonal: 'export' }
                      // },
                      // {
                      //     extend: 'pdfHtml5',
                      //      title: 'Daily Sales Report',
                      //      message: '',
                      //     exportOptions: { orthogonal: 'export' }
                      // }
                  ]
              } );
    });
  </script>
@endpush 
