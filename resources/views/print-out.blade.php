
@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1)
    <script>window.location = "dashboard";</script>
  @endunless
@endif


@extends('layouts.master-layout')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<button class="btn btn-warning text-white" onclick="printJS({ printable: 'printable', type: 'html', header: '' })"><i class="fa fa-print"></i> Print</button>
			<hr>
		</div>
	</div>
</div>
<div class="container" id="printable">
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered">
				<thead>
					<tr>
						<td colspan="2"><h6 class="text-muted">{{ $store->name }} <small> Target Qouta</small> : ₱{{ number_format($store->qouta, 2) }}</h6></td>
					</tr>
					<tr>
						<th colspan="2" class="text-center">Part I EXPENSES REPORT</th>
					</tr>
					<tr>
						<th>Expeses Discription</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					@if(!empty($expenses))
						@foreach($expenses as $value)
							<tr>
								<td>{{ $value['details'] }}</td>
								<td>₱{{ number_format($value['amount']) }}</td>
							</tr>
						@endforeach
					@endif
				</tbody>
				<tfoot>
					<tr>
						<td><span class="pull-right  text-muted">TOTAL&nbsp;EXPENSES:</span> </td>
						<td><i class="fa fa-rub"></i>₱{{number_format(round($total_expenses))}}</span></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2" class="text-center">Part II COMMISION COMPUTATION</th>
					</tr>
					<tr>
						<th>Expeses Discription</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Qouta Commision</td>
						<td>
							@if($total_sales > $store->qouta)
								₱{{ number_format($qouta_percent = (1 / 100) * $qouta_bonus, 2) }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Equipment Commision</td>
						<td>
							@if($total_sales > $store->qouta)
								₱{{ number_format($equipment_bonus, 2) }}
							@endif
						</td>
					</tr>
					<tr>
						<td>Total Pin Money</td>
						<td>
							@if($total_sales > $store->qouta)
								₱{{number_format( $pin_bonus, 2) }}
							@endif
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td><span class="text-muted pull-right" style="text-align: right;">TOTAL&nbsp;COMMISION: </span></td>
						<td><i class="fa fa-rub"></i>
							
							@if($total_sales > $store->qouta)
							@php 
								$total_bonus = array($pin_bonus, $qouta_percent);
							@endphp
								₱{{ number_format(array_sum($total_bonus), 2) }}
							@endif
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="col-sm-6">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2" class="text-center">Part III SUMMARY OF SALES</th>
					</tr>
					
				</thead>
				<tbody>
					<tr>
						<td>Total Sales</td>
						<td>₱{{ $sales }}</td>
					</tr>
					<tr>
						<td>Total Expenses</td>
						<td>₱{{ number_format($total_expenses, 2) }}</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td>
							<span class="pull-right  text-muted" style="text-align: right;">GRAND&nbsp;TOTAL: </span>
						</td>
						<td>
							<i class="fa fa-rub"></i>

							@php
							$grand_total = $sales - $total_expenses;
							@endphp
							<span id="grand-total">₱{{ number_format($grand_total, 2) }}</span>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered" id="cashFund">
				<thead>
					<tr>
						<th colspan="20" class="text-center">Part IV CASH FUND BREAKDOWN </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><i>P1000</i></td>
						<td><input type="number" id="1k" name="1k" val="1000" placeholder="x"></td>
						<td><span>=</span></td>
						<td><i>P</i><input type="number" id="1k_total"></td>
						
						<td><i>P20</i></td>
						<td><input type="number" name="20" placeholder="x"></td>
						<td><span>=</span></td>
						<td><i>P</i><input type="number" id="20_total"></td>


						<td><h6>Short in Cash:</h6></td>
						<td><i>P</i><input type="number" id="short-in-cash"></td>
					</tr>
					<tr>
						<td><i>P500</i></td>
						<td><input type="number" name="500" placeholder="x"></td>
						<td><span>=</span></td>
						<td><i>P</i><input type="number" id="500_total"></td>

						<td><i>P10</i></td>
						<td><input type="number" name="10" placeholder="x"></td>
						<td><span>=</span></td>
						<td><i>P</i><input type="number" id="10_total"></td>


						<td><h6>Over in Cash :</h6></td>

						<td><i>P</i><input type="number" id="over-in-cash"></td>
					</tr>
					<tr>
						<td><i>P100</i></td>
						<td><input type="number" name="100" placeholder="x"></td>
						<td><span>=</span></td>
						<td><i>P</i><input type="number" id="100_total"></td>

						<td><i>P5</i></td>
						<td><input type="number" name="5" placeholder="x"></td>
						<td><span>=</span></td>
						<td><i>P</i><input type="number" id="5_total"></td>
						<td><h6>Sealed Cash :</h6></td>
						<td><i>P</i><input type="number" id="sealed" value=""></td>
					</tr>
					<tr>
						<td><i>P50</i></td>
						<td><input type="number" name="50" placeholder="x"></td>
						<td><span>=</span></td>
						<td><i>P</i><input type="number" id="50_total"></td>

						<td><i>P1</i></td>
						<td><input type="number" name="1" placeholder="x"></td>
						<td><span>=</span></td>
						<td><i>P</i><input type="number" id="1_total"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2" class="text-center">Part V ACKNOWLEDGEMENT </th>
					</tr>
					
				</thead>
				<tbody>
					<tr style="border: none;"> 
						<td style="border-right: 1; border-bottom: 0;">Cashier</td>
						<td style="border-bottom: 0;">Witness</td>
					</tr>
					<tr>
						<td class="text-center text-muted" style="text-align: center; border-top: 0; ">
							<p style="margin-top: 30px; margin-left: 80px; ">
								<span style="border-top: 1px solid; margin-top: 10px;">Signature over Printed Name</span></p>
						</td>
						<td class="text-center text-muted" style="text-align: center; border-top: 0; ">
							<p style="margin-top: 30px;  margin-left: 80px; ">
								<span style="border-top: 1px solid; margin-top: 10px; " >Signature over Printed Name</span></p>
						</td>
						
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2" class="text-center" style="border-bottom: 0;">Part V Remarks</th>
					</tr>
					<tr>
						<th colspan="2" style="border: 0; padding: 50px;"></th>
						
					</tr>
				</thead>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection