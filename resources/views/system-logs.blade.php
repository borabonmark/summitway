@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1)
    <script>window.location = "dashboard";</script>
  @endunless
@endif


@extends('layouts.master-layout')


@section('content')
    
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Transactions</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Transaction Logs
          <br>
          <br>

          @if (Auth::user()->store_id === 1)
            <form action="{{ route('transactions.store') }}" method="post">
              @csrf
              @method('POST')
              <div class="row">
                  <div class="col-sm-3">
                    <select name="store_id" id="" class="form-control">
                      <option value=" "disabled selected>Please select store</option>
                      @foreach ($stores as $item)
                          <option value="{{ $item->id }}"
                            @php
                                $storeId = $store->id ?? NULL;
                            @endphp
                            @if(old('store_id',$item->id) == $storeId)
                                selected
                            @endif 
                            >{{ $item->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  
                  <div class="col-sm-2">
                      <button type="submit" class="btn btn-primary form-control">Filter</button>
                  </div>
              </div>
          </form>
          @endif
          
            
        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                      <th>Store</th>
                      <th>User</th>
                      <th>Log Details</th>
                      <th>Log DateTime</th>
                    </tr>
                  </thead>
              <tbody>
                @if(!empty($logs))
                  @foreach($logs as $item)
                      <tr>
                          <td>{{ $item->store->name }}</td>
                          <td>{{ $item->user->name }}</td>
                          <td>
                              @if (!empty($item->sales))
                                {{ $item }}
                              @else
                                @if (!empty($item->itemlist))
                                  {{ $item }}
                                @else
                                  @if (!empty($item->customer))
                                    {{ $item }}
                                  @else
                                      
                                  @endif
                                @endif
                              @endif

                          </td>
                          <td>
                              {{ $item->created_at }}
                          </td>
                      </tr>   
                    @endforeach
                  @endif
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>
@endsection

@push('script')
<script type="text/javascript">
      $(document).ready(function() {
        $('#dataTable').DataTable();
        // $('.export').datepicker();
      });
  </script>

{{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
      $('.export').datepicker();
    });
</script> --}}
@endpush 