@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1|| Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif
@extends('layouts.master-layout')
@section('content')
  <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Inventory</li>
      </ol>
     
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Inventory Record
          @if(Auth::user()->store_id===1)
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#insertModal">Add new</button>
          @else
          <button type="button" class="btn btn-danger btn-sm text-white pull-right" data-toggle="modal" data-target="#resetModal">Ask Reset</button>
          @endif
        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="inventoryList" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  {{-- <th>Store</th>
                  <th>Itemlist</th> --}}
                  <th>Barcode</th>
                  @if (Auth::user()->type == 1)
                    <th>Name</th>
                    <th>Details</th>
                    <th>Category</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Bonus&nbsp;Type</th>
                    <th>Bonus&nbsp;Amount</th>
                  @else

                    <th>Quantity</th>
                  @endif
                  
                </tr>
              </thead>
              
              <tbody>
                @foreach($orderproducts as $item)
                  @if(!empty($item->itemlist->id))  
                    <tr>
                      <td>{{ $item->itemlist->id }}</td>
                        <td>{{ $item->itemlist->barcode }}</td>

                        @if (Auth::user()->type == 1)
                          <td>{{ $item->itemlist->name }}</td>
                          <td>{{ $item->itemlist->desc }}</td>
                          <td>{{ $item->itemlist->category }}</td>
                          <td>{{ $item->qty }}</td>
                          <td>{{ $item->itemlist->price }}</td> 

                          @if($item->itemlist->bonus_type === 1)
                            <td>Pin Money</td>
                            <td>{{ $item->itemlist->bonus_amount }}</td>
                          @elseif($item->itemlist->bonus_type === 2)
                            <td>Equipment</td>
                            <td>{{ $item->itemlist->bonus_amount }}</td>
                          @else
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          @endif
                        @else
                        <td>{{ $item->qty }}</td>
                        @endif
                        
                    </tr>
                @endif

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>


  <!-- Button trigger modal -->  

    <!-- Insert -->
    <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        
        <form role="form" method="post" action="{{url('inventory')}}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             
                {{csrf_field()}}
                <label>Item name</label>
                <br>
                <select class="selectpicker" data-live-search="true" name="item">
                  @foreach($items as $item)
                  <option data-tokens="{{$item->name}}" value="{{$item->id}}">
                  {{$item->barcode}} - {{$item->name}}
                  </option>
                  @endforeach
                </select>
                <br><br>
                <input class="form-control" placeholder="QTY" name="qty" type="number">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <input type="submit" name="submit" class="btn btn-primary">    
          </div>
        </form>

        </div>
      </div>
    </div>

     <!-- Reset -->
     <div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="{{url('reset')}}" method="post">
          <div class="modal-body">
            @csrf
              <textarea name="notes" class="form-control">We would like to reset our inventory.</textarea>
              <label></label>
              <input type="hidden" name="store_id" value="{{Auth::user()->store_id}}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button class="btn btn-danger" type="submit">Confirm</button>
          </div>
          </form>
        </div>
      </div>
    </div>

@endsection
@push('script')
 <script type="text/javascript">
      $(document).ready(function() {
        $('#inventoryList').DataTable( {
                  dom: 'Bfrtip',
                  buttons: [
                      {
                          extend: 'excelHtml5',
                          title: '',
                          message: '',
                          exportOptions: {  columns: [0, 1, 2, 3, 4, 5, 6, 7, 8 ]  }
                      },
                      {
                          extend: 'pdfHtml5',
                           title: 'Inventory List',
                           message: '',
                          exportOptions: {  columns: [ 1, 2, 6 ] }
                      }
                  ]
              } );
    });
  </script>
@endpush 