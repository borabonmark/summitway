@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1 || Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif

@extends('layouts.master-layout')


@section('content')
    
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">All Returned Item</li>
      </ol>
 
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> All Returned Item
          <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#insertModal">Add new</button>
      </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Barcode</th>
                  <th>QTY</th>
                  <th>Price</th>
                  <th>Notes</th>
                  <th>Created At</th>
                  <th>Action</th>                  
                </tr>
              </thead>
              <tbody>
                @foreach($returns as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->barcode }}</td>
                        <td>{{ $item ->qty }}</td>
                        <td>{{ $item->price }}</td>  
                        <td>{{ $item->notes }}</td>  
                        <td>{{ $item->created_at }}</td>                                        
                        <td>
                          @if ($item->is_approved != true)
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal{{$item->id}}">Update</button>
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{$item->id}}">Delete</button>
                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#approvedModal{{$item->id}}">Approve</button>
                          @endif


                          <!-- delete -->
                          <div class="modal fade" id="approvedModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              
                              <form role="form" method="post" action="{{ route('return.approved')}}">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="return_id" value="{{  $item->id }}">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                      Are you sure?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    
                                  <button class="btn btn-success" type="submit">Approved</button>    
                                </div>
                              </form>

                              </div>
                            </div>
                          </div>
                            
                            <!-- delete -->
                            <div class="modal fade" id="deleteModal{{$item['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                
                                <form role="form" method="post" action="{{action('ReturnItemController@destroy', $item['id'])}}">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                        @csrf
                                        Are you sure?
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>    
                                  </div>
                                </form>

                                </div>
                              </div>
                            </div>


                            

                            <!-- Edit -->
                            <div class="modal fade" id="editModal{{$item['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                
                                <form role="form" method="POST" action="{{ route('return.destroy' , $item['id'])}}">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Update Item</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">                             
                                        @csrf
                                        <label>Barcode</label>
                                        <input class="form-control" placeholder="Name" name="barcode" type="text" value="{{$item['barcode']}}" required="required">
                                        {{-- <label>Name</label>
                                        <input class="form-control" placeholder="name" name="name" type="text" value="{{$item['name']}}" required="required"> --}}
                                        <label>QTY</label>
                                        <input class="form-control" placeholder="Quantity" name="old_qty" type="number" value="{{$item['qty']}}" readonly>
                                        <label></label>
                                        <input class="form-control" placeholder="Update Quantity" name="qty" type="number" required="required">
                                        <label>Price</label>
                                        <input class="form-control" placeholder="Item Price" name="price" type="number" value="{{$item['price']}}" step=".01" required="required">
                                        <label>Price</label>
                                        <textarea name="notes" class="form-control"  id="" cols="5" rows="3">{{ $item->notes }}</textarea>
                                      </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    @method('PATCH')
                                    <button class="btn btn-danger test" type="submit">Update</button> 
                                  </div>
                                </form>
                                </div>
                              </div>
                            </div>
                        </td> 
                    </tr>

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>


  <!-- Button trigger modal -->  

    <!-- Insert -->
    <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        
        <form role="form" method="post" action="{{ route('return.store') }}">
          @csrf
          @method('POST')
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Create New Return</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">             
                
                <label></label>
                @auth 
                  <input class="form-control"  name="store_id" type="hidden" value="{{Auth::user()->store_id}}" required="required">
                @endauth
                <input class="form-control" placeholder="Barcode" name="barcode" type="text" required="required">
                <label></label>
                {{-- <input class="form-control" placeholder="Name" name="name" type="text" required="required"> --}}
                <label></label>
                <input class="form-control" placeholder="qty" name="qty" type="number" required="required">
                <label></label>
                <input class="form-control" placeholder="price" name="price" type="number" step=".01" required="required">
                <label></label>
                <textarea name="notes" class="form-control" id="" cols="5" rows="3" placeholder="Notes"></textarea>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>   
          </div>
        </form>

        </div>
      </div>
    </div>
@endsection

