@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1 || Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif

@extends('layouts.master-layout')


@section('content')
    
<div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Order</li>
      </ol>
   
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Orders
          <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#insertModal">Make order</button>
        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>StoreName</th>
                  <th>Status</th>
                  <th>Notes</th>
                </tr>
              </thead>
              <tbody>
                @foreach($orders as $order)
                @php
                    $order = (object)$order;
                @endphp
                    <tr>
                        <td>{{ $order->id }}</td>                        
                        <td>{{ ($order->store->name ?? NULL)}}</td>
                        <td>
                        
                        @if($order->status=='pending')
                          <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#confirmModal{{$order->id}}">{{ ucwords($order->status) }}</button>
                        @elseif($order->status =='processing')
                         <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirmModal{{$order->id}}">{{ ucwords($order->status) }}</button>
                        @elseif($order->status == 'canceled')
                          <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmModal{{$order->id}}">{{ ucwords($order->status) }}</button>
                        @elseif($order->status == 'delivering')
                          <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#confirmModal{{$order->id}}">In Transit</button>
                        @else
                          <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#confirmModal{{$order->id}}">{{ ucwords($order->status) }}</button>
                        @endif

                        
                          <!-- Edit -->
                          <div class="modal fade" id="confirmModal{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              
                              <form role="form" method="post" action="{{action('OrderController@update', $order->id)}}">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalCenterTitle">Order information</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">

                                      {{csrf_field()}}
                                      <h3>
                                      <center>
                                      <a href="orderproduct/{{$order['id']}}">
                                      View details
                                      </a>
                                      </h3>
                                      </center>
                                      <br>
                                      @unless($order->status=='delivered')
                                        @if(Auth::user()->store_id===1)
                                        <select name="status" class="form-control" onchange="this.form.submit()">
                                            <option selected="selected">{{$order->status}}</option>
                                            <option value="pending">Pending</option>
                                            <option value="processing">Processing</option>
                                            <option value="delivering">Delivering</option>  
                                            <option value="canceled">Cancel</option>                                       
                                        </select>
                                        @endif
                                      @endunless
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  @method('PATCH')
                                  <!-- <button class="btn btn-danger" type="submit">Confirm delivery</button>    -->
                                </div>
                              </form>
                              </div>
                            </div>
                          </div>
                        
            
                        </td>   
                        <td>{{$order->notes}}</td>                   
                    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>

          <!-- Insert -->
          <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
              
              <form role="form" method="post" action="{{url('order')}}">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">New data</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">      
                  @csrf
                    @if(Auth::user()->store_id===1)                      
                      <label>Select Store</label>
                      <select class="form-control" name="store_id">
                        @foreach($stores as $store)
                        <option value="{{ $store->id }}">{{ $store->name }}</option>
                        @endforeach
                      </select>
                    @else
                      <input type="hidden" name="store_id" value="{{Auth::user()->store_id}}">
                    @endif 

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button class="btn btn-danger" type="submit">Create</button>   
                </div>
              </form>

              </div>
            </div>
          </div>



@endsection

@push('script')
 {{-- <script type="text/javascript">
      $(document).ready(function() {
        $('#dataTable').DataTable({
          "order": [[ 1, "desc" ]]
        });
      });
  </script> --}}
@endpush 