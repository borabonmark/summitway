@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1 || Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif

@extends('layouts.master-layout')


@section('content')
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Items</li>
      </ol>
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <form method="post" action="{{ url('search') }}" >
                @csrf
                <div class="input-group mb-3">
                  <input type="text" name="search" class="form-control" placeholder="Barcode.."  required>
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" >Search</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>


      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Product list

        
          <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#insertModal">Add new</button>
      </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="inventoryList" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Barcode</th>
                  <th>Name</th>
                  <th>Details</th>
                  <th>Category</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Bonus&nbsp;Type</th>
                  <th>Bonus&nbsp;Amount</th>
                  <th>Action</th>                  
                </tr>
              </thead>
              <tbody>
                @foreach($itemlist as $item)
                    <tr>
                        <td>{{$item['id']}}</td>
                        <td>{{$item['barcode']}}</td>
                        <td>{{$item['name']}}</td>
                        <td>{{$item['desc']}}</td>
                        <td>{{$item['category']}}</td>
                        <td>{{$item['qty']}}</td>
                        <td>{{$item['price']}}</td> 

                        @if($item['bonus_type']===1)
                          <td>Pin Money</td>
                          <td>{{ $item['bonus_amount'] }}</td>
                        @elseif($item['bonus_type']===2)
                          <td>Equipment</td>
                          <td>{{ $item['bonus_amount'] }}</td>
                        @else
                          <td>None</td>
                          <td>None</td>
                        @endif
                        
                                                             
                        <td>
                          @if (Auth::user()->store_id === 1 && Auth::user()->type === 1)
                            <a href="{{ route('itemlist.show' , $item['id'])}}" class="btn btn-warning btn-sm text-white">Update</a>
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{$item['id']}}">Delete</button>
                          @endif
                            
                            <!-- delete -->
                          <div class="modal fade" id="deleteModal{{$item['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              
                              <form role="form" method="post" action="{{ route('itemlist.destroy' , $item['id'])}}">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                      @csrf
                                      Are you sure?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    @method('DELETE')
                                  <button class="btn btn-danger" type="submit">Delete</button>    
                                </div>
                              </form>

                              </div>
                            </div>
                          </div>
                        </td> 
                    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">
          {{ $itemlist->links()}}
        </div>
      </div>
    </div>

    <table style="display: none;" id="printDelivery">
      <thead>
        <tr>
          <th>Description</th>
          <th>Quantity</th>
          <th>Barcode</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  <!-- Button trigger modal -->  

    <!-- Insert -->
    <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        
        <form role="form" method="post" action="{{url('itemlist')}}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Create New Product</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">             
                @csrf
                <label></label>
                <input class="form-control" placeholder="Barcode" name="barcode" type="text" required="required">
                <label></label>
                <input class="form-control" placeholder="Name" name="name" type="text" required="required">
                <label></label>
                <input class="form-control" placeholder="description" name="desc" type="text" required="required">
                <label></label>
                <input class="form-control" placeholder="category" name="category" type="text" required="required">
                <label></label>
                <input class="form-control" placeholder="Initial qty" name="qty" type="number" required="required">
                <label></label>
                <input class="form-control" placeholder="price" name="price" type="number" step=".01" required="required">
                <hr>
                <label class="radio-inline"><input type="radio" name="bonus" value="1"> Pin Money</label>
                <label class="radio-inline"><input type="radio" name="bonus" value="2"> Equipment</label>
                <input class="form-control bonus_amount" placeholder="pin money amount" name="bonus_amount" type="number" id="pin-amount">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <input type="submit" name="submit" class="btn btn-primary">    
          </div>
        </form>

        </div>
      </div>
    </div>
@endsection
@push('script')
 <script type="text/javascript">
      $(document).ready(function() {
        //console.log('test');
        $("input[type=radio]").click(function () {
            if($(this).prop("checked")) { console.log("checked!"); }
            $('input[name="bonus_amount"]').prop('required',true);
        });
          
        // $('#inventoryList').DataTable( {
        //         "processing": true,
        //         "serverSide": true,
        //         "ajax": "get-product-list",
        //         // "deferLoading": 57
        //           // dom: 'Bfrtip',
        //           // buttons: [
        //           //     {
        //           //         extend: 'excelHtml5',
        //           //         title: 'Inventory List',
        //           //         message: '',
        //           //         exportOptions: {  columns: [ 1, 3, 5 ]  }
        //           //     },
        //           //     {
        //           //         extend: 'pdfHtml5',
        //           //          title: 'Inventory List',
        //           //          message: '',
        //           //         exportOptions: {  columns: [ 1, 3, 5 ] }
        //           //     }
        //           // ]
        //       } );
    });
    
  </script>
@endpush 

