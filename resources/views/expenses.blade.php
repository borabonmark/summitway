@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1|| Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif


@extends('layouts.master-layout')


@section('content')
    
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Expenses</li>
      </ol>

      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Expenses
          
          <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#insertModal">Add new</button>
         
        </div>

        <div class="card-body">
          <div class="table-responsive">

            <table class="table table-bordered" id="expensesList" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Amount</th>
                  <th>Details</th> 
                  <th>Date/Time</th> 
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Amount</th>
                  <th>Details</th>  
                  <th>Date/Time</th>                           
               </tr>
              </tfoot>
              <tbody>
                @if(!empty($expenses))
                  @foreach($expenses as $item)
                      <tr>
                          <td>{{$item['id']}}</td>
                          <td>{{$item['amount']}}</td>
                          <td>{{$item['details']}}</td>
                          <td>{{$item['created_at']}}</td>
                      </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">
          <h4 class="pull-right"> Total Expenses: P{{number_format($totalExpenses)}}</h4>
        </div>
      </div>
    </div>


  <!-- Button trigger modal -->  

    <!-- Insert -->
    <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        
        <form role="form" method="post" action="{{url('expenses')}}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Add new Expenses</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                @csrf
                <label>Amount</label>
                <input type="number" name="amount" class="form-control" required="true">
                <label>Details</label>
                <textarea name="details" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <input type="submit" name="submit" class="btn btn-primary">    
          </div>
        </form>

        </div>
      </div>
    </div>

@endsection
@push('script')
 <script type="text/javascript">
      $(document).ready(function() {
        $('#expensesList').DataTable( {
                  dom: 'Bfrtip',
                  buttons: [
                      {
                          extend: 'excelHtml5',
                          title: 'Inventory List',
                          message: '',
                          exportOptions: {  columns: [ 1, 3, 5 ]  }
                      },
                      {
                          extend: 'pdfHtml5',
                           title: 'Inventory List',
                           message: '',
                          exportOptions: {  columns: [ 1, 3, 5 ] }
                      }
                  ]
              } );
    });
  </script>
@endpush 