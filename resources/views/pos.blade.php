@extends('layouts.master-layout')
@section('content')
    
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">POS</li>
      </ol>

      <div class="row">
        <div class="col-lg-6">        
            <div class="card mb-3">
              <div class="card-header">
                <i class="fa fa-bar-chart"></i> Details price</div>
              <div class="card-body">

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Total amount</span>
                  </div>
                  <input type="text" class="form-control" id="totalAmount" value="{{$invoice->total}}" readonly>
                  <div class="input-group-append">
                    <span class="input-group-text">PHP</span>
                  </div>
                </div>
                                    
                    @unless($invoice->confirmed==1)

                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Quantity</span>
                      </div>
                      <input type="number" class="form-control" id="qty" placeholder="Quantity" value="1">
                      <div class="input-group-append">
                        <span class="input-group-text"></span>
                      </div>
                    </div>

                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Discounted </span>
                      </div>
                      <input type="number" class="form-control" id="price" placeholder="Price">
                      <div class="input-group-append">
                        <span class="input-group-text">PHP</span>
                      </div>
                    </div>

                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Enter barcode</span>
                      </div>
                      
                    <input type="text" class="form-control" id="barCode" placeholder="Barcode"> 
                     
                    </div>          
                    @endunless

                    <br>                  
                    <button style="display: none;" id="newItem">Add</button>
                    <br>

                    @php
                        $current_time = Carbon\Carbon::createFromTime(Carbon\Carbon::now()->hour)->format('H');
                        $now  = Carbon\Carbon::createFromFormat(' H', $current_time);
                        $start_time = Carbon\Carbon::createFromFormat(' H:i', Auth::user()->store->closing_time);
                        $end_time = Carbon\Carbon::createFromFormat('H:i','23:59');

                    @endphp

                    @if ($now->toTimeString() >= $start_time->toTimeString() && $now->toTimeString() <= $end_time->toTimeString())
                       <span class="alert alert-warning">Processing is not Available at the moment!</span>
                    @else
                      <button class="btn btn-success" type="submit" id="newCustomer" data-toggle="modal" data-target="#insertModal">Procceed</button> 
                    @endif                 
              </div>
              <div class="card-footer small text-muted"></div>
            </div>                        
        </div>

        <div class="col-lg-6">        
            <div class="card mb-3">
              <div class="card-header">
                <i class="fa fa-bar-chart"></i> Invoice item</div>
              <div class="card-body" id="items">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">Barcode</th>
                      <th scope="col">Item</th>
                      <th scope="col">Qty</th>
                      <th scope="col">Price</th>
                      <th scope="col">Amount</th>                      
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($items as $item)   
                    <tr>                 
                        <th scope="row" class="barcode-item">{{$item->barcode}}</th>                       
                        <td>{{$item->itemlist->name}}</td>
                        <td>{{$item->qty}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->qty*$item->price}}</td>                      
                    </tr>
                  @endforeach
                    <tr>                 
                        <th scope="row">Total amount</th>                       
                        <td></td>
                        <td></td>
                        <td></td>
                        <th scope="row">{{$invoice->total}} Php</th>                      
                    </tr>
                  </tbody>
                </table>

              </div>
              <div class="card-footer small text-muted"></div>
            </div>                        
        </div>
      </div>
      
<!-- Insert -->
    <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        
        <form role="form" method="post" action="{{url('invoice')}}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Create tem</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> 
          <div class="modal-body">             
                @csrf
                <button type="button" class="btn btn-primary" style="cursor: pointer;" onclick="printJS({ printable: 'items', type: 'html', header: 'Store POS invoice #{{$invoice->id}}' })">
                      <i class="fa fa-print"> Print Receipt</i>
                </button>
                
                <input type="hidden" name="id" value="{{$invoice->id}}">
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            @unless($invoice->confirmed==1)
            <input type="submit" name="submit" class="btn btn-success" value="Finish transaction">
            @endunless    
          </div>
        </form> 

        </div>
      </div>
    </div>
<!-- 
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#notification-modal">Edit</button>
-->
  <div class="modal fade" id="notification-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle" >Out of Stock!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p class="text-danger"></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
            </div>
          </div>
        </div>
      </div> 


      @csrf    
      <input type="hidden" name="customer_id" value="{{$invoice->id}}">

 
        <script>
        $(document).ready(function(){
          var isNotError = true;
            $('#barCode').focus();
            
            $("#newItem").click(function(event) {

              $('#items table tr').each(function() {

                var item = $(this).find(".barcode-item");

                if(item.text() == $('#barCode').val())
                {
                  console.log('yaaay')
                  
                  isNotError = false;
                }

              });

              if(isNotError == false)
              {
                alert('Barcode is on the list already!');
                return false;
              }


              var barcode = $('#barCode').val();
              var qty = $('#qty').val();
              var price = $('#price').val();
              var csrf = $('input[name="_token"]').val(); 
              var id = $('input[name="customer_id"]').val();

              

                $.post('/pos', {'barcode': barcode, 'price': price, 'qty': qty, '_token': csrf, 'customer_id': id}, function(data){
                  
                  console.log(data);

                  if(data.error) {

                    $('#notification-modal').modal('show');
                    $('#notification-modal .modal-body p').html(data.error);
                    $('#totalAmount').val(0);
                  } else {
                    $('#totalAmount').val(data);
                    $('#items').load(location.href + ' #items');
                  }
                });        
            });

            $("#barCode").keyup(function(event) {
                if (event.keyCode === 13) {
                    $("#newItem").click();
                    $('#barCode').val('');
                    $('#qty').val('1');
                    $('#barCode').focus();
                }
            });

        });
        </script>
@endsection

