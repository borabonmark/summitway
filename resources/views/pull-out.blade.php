@if(auth::check())
  @unless (Auth::user()->store_id===1 || Auth::user()->type===1 || Auth::user()->type===2)
    <script>window.location = "dashboard";</script>
  @endunless
@endif

@extends('layouts.master-layout')


@section('content')
    
        <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Pull Outs Items</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Pull Outs Item
          @if (Auth::user()->store_id != 1)
          <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#insertModal">Add new</button>
          
          @endif
          <a href="{{ url('pull-outs/export/'. Auth::user()->store_id) }}" class="btn btn-success btn-sm mr-2 pull-right">Export All Pullouts</a>
      </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Store</th>
                  <th>Barcode</th>
                  <th>QTY</th>
                  <th>Price</th>
                  
                  <th>Description</th>
                  <th>Remarks</th>
                  <th>Date Approved</th> 
                  <th>Action</th>                
                </tr>
              </thead>
              <tbody>
                @foreach($pullOuts as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->store->name }}</td>
                        <td>{{ $item->itemlist->barcode }}</td>
                        <td>{{ $item ->qty }}</td>
                        <td>{{ $item->price }}</td>
                        
                        <td>{{ $item->itemlist->desc }}</td>
                        <td>{{ $item->remarks }}</td>
                        <td>
                            @if ($item->is_approved)
                                {{ $item->updated_at }}
                            @endif
                        </td>                               
                        <td>
                          @if ($item->is_approved != true && Auth::user()->store_id != 1)
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal{{$item->id}}">Update</button>
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{$item->id}}">Delete</button>
                            
                          @endif
                          @if (Auth::user()->store_id === 1 && $item->is_approved != true)
                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#approvedModal{{$item->id}}">Approve</button>
                          @endif


                          <!-- delete -->
                          <div class="modal fade" id="approvedModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                              <div class="modal-content">
                              
                              <form role="form" method="post" action="{{ route('pull-outs.approved') }}">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="pullout_id" value="{{  $item->id }}">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                      Are you sure?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    
                                  <button class="btn btn-success" type="submit">Approved</button>    
                                </div>
                              </form>

                              </div>
                            </div>
                          </div>
                            
                            <!-- delete -->
                            <div class="modal fade" id="deleteModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                
                                <form role="form" method="post" action="{{ route('pull-outs.destroy', $item->id)}}">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Confirm action</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                        @csrf
                                        Are you sure?
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>    
                                  </div>
                                </form>
                                </div>
                              </div>
                            </div>

                            <!-- Edit -->
                            <div class="modal fade" id="editModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                
                                <form role="form" method="POST" action="{{ route('pull-outs.update' , $item->id)}}">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Update Item</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">                             
                                        @csrf
                                        <label>Barcode</label>
                                        {{-- <input type="hidden" name="barcode" value="{{  }}"> --}}
                                        <input class="form-control" placeholder="Name" readonly name="barcode" type="text" value="{{ old('barcode', $item->itemlist->barcode) }}" required="required">
                                        <label>QTY</label>
                                        <input class="form-control" placeholder="Quantity" name="old_qty" type="number" value="{{$item['qty']}}" readonly>
                                        <label></label>
                                        <input class="form-control" placeholder="Update Quantity" name="qty" type="number" required="required">
                                        <label>Price</label>
                                        <input class="form-control" placeholder="Item Price" name="price" type="number" value="{{ $item->price }}" step=".01" required="required">
                                        <label for="">Remarks</label>
                                        <textarea name="remarks" id="" cols="30" rows="10" class="form-control">{{ $item->remarks }}</textarea>
                                    </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    @method('PATCH')
                                    <button class="btn btn-danger test" type="submit">Update</button> 
                                  </div>
                                </form>
                                </div>
                              </div>
                            </div>
                        </td> 
                    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>

    <!-- Insert -->
    <div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        
        <form role="form" method="post" action="{{ route('pull-outs.store') }}">
          @csrf
          @method('POST')
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Create New Pull Out</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">             
                
                <label></label>
                @auth 
                  <input class="form-control"  name="store_id" type="hidden" value="{{Auth::user()->store_id}}" required="required">
                @endauth
                <input class="form-control" placeholder="Barcode" name="barcode" type="text" required="required">
                <label></label>
                {{-- <input class="form-control" placeholder="Name" name="name" type="text" required="required"> --}}
                <label></label>
                <input class="form-control" placeholder="qty" name="qty" type="number" required="required">
                <label></label>
                <input class="form-control" placeholder="price" name="price" type="number" step=".01" required="required">
                <label for="">Remarks</label>
                <textarea name="remarks" id="" cols="30" rows="10" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>   
          </div>
        </form>

        </div>
      </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
  $(document).ready(function() {
    $('#dataTable').DataTable(
      {
        "order": [[ 1, "desc" ]]
      }
    );
  });
  </script>
@endpush
