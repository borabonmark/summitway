<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PullOut extends Model
{
    public function itemlist()
    {
        return $this->belongsTo(Itemlist::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
