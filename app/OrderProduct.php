<?php

namespace App;


use App\Itemlist;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function itemlist()
    {
        return $this->belongsTo(Itemlist::class);
    }

    public function itemmeta()
    {
        return $this->belongsTo(item_meta::class);
    }
}
