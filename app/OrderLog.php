<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'order_id', 'type', 'message'
    ];

    /**
     * Types
     */
    const TYPE_STATUS_UPDATE = 'status update';

    public static $types = [
        self::TYPE_STATUS_UPDATE
    ];

    /**
     * Get the user 
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
