<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function orderproduct()
    {
        return $this->hasMany(OrderProduct::class);
    }
}
