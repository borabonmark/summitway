<?php

namespace App;

use App\Store;
use Illuminate\Database\Eloquent\Model;


class Customer extends Model
{
    public function store()
    {
    	return $this->belongsTo(Store::class);
    }

    public function sales()
    {
    	return $this->hasMany(Sale::class);
    }


    public static function deleteEmptyChild()
    {
        $noChild = self::doesnthave('sales')->get();
        foreach($noChild as $item)
        {
            $item->delete();
        }
        return true;
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

}
