<?php

namespace App;


use App\OrderProduct;
use Illuminate\Database\Eloquent\Model;

class Itemlist extends Model
{

    public function orders()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function storeStocks()
    {
        return $this->hasMany(item_meta::class);
    }
}
