<?php

namespace App\Export;
use App\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;

class DataExport implements FromCollection
{
	protected $id;

	public function __construct($id){
		 $this->id = $id;
	}

    public function collection()
    {
        return Customer::where('store_id', $this->id)->whereDate('created_at', Carbon::today())->where('confirmed', true)->get();
    }
}