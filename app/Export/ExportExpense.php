<?php

namespace App\Export;

use App\Expense;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\Resources\ExpenseResource;
use Session;

class ExportExpense implements FromCollection, WithHeadings
{

    protected $store_id;
    protected $filter_date;

    function __construct($filter_date, $store_id)
    {
        $this->store_id = $store_id;
        $this->filter_date = $filter_date;
    }


    public function collection()
    {
        Session::put('store_id', $this->store_id);
        Session::put('filter_date', $this->filter_date);

        $expenses = ExpenseResource::collection(Expense::where('store_id', $this->store_id)->whereDate('created_at', $this->filter_date)->get());

        return $expenses;
    }

    public function headings(): array
    {
        return [
            // 'Store',
            'Expenses Amount',
            'Expenses Details',
            'Expenses Transaction Time',
        ];
    }
}