<?php 
namespace App\Export;

use App\Customer;
use App\Expense;
use App\item_meta;
use App\Store;
use App\Sale;
use App\TransactionReport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use DB;
use App\Http\Resources\SalesResource;
use Session;

class ExcelExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    protected $filter_date;
    protected $store_id;

    function __construct($filter_date, $store_id) {

        $this->filter_date = $filter_date;
        $this->store_id = $store_id;
    }

    public function collection()
    {
        Session::put('store_id', $this->store_id);
        Session::put('filter_date', $this->filter_date);

        $customers = Customer::where('store_id', $this->store_id)->where('confirmed',true)->whereDate('created_at', $this->filter_date)->pluck('id')->toArray();
        $data = SalesResource::collection(Sale::whereIn('customer_id', $customers)->orderBy('id','desc')->get());
       
        

        return $data;
    }

    public function headings(): array
    {
        $summary = [];

        if($this->filter_date == Carbon::today()->toDateString())
        {
            $summary = [
                
                'Beginning Qty',
                'Total Delivery',
                'Total Pull Out',
                'Total Sold',
                'Ending Qty'
            ];
        }
        
        $sales = [
            'Total Bonus',
            'Barcode',
            'Item Description',
            'Price',
            'Total',
            'Qty',
            'OS#',
            'OR#',
            'Transaction Time'
        ];


        return array_merge($sales, $summary);
    }
}