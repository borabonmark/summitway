<?php

namespace App\Export;

use App\PullOut;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\Resources\PullOutResource;


class ExportPullOut implements FromCollection, WithHeadings
{

    protected $store_id;

    function __construct($store_id)
    {
        $this->store_id = $store_id;
    }


    public function collection()
    {
        return PullOutResource::collection(PullOut::where('store_id', $this->store_id)->where('is_approved', false)->orderBy('id', 'DESC')->get());
    }

    public function headings(): array
    {
        return [
            'Store',
            'Barcode',
            'Quantity',
            'Description',
            'Remarks',
            'Date'
        ];
    }
}