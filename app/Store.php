<?php

namespace App;

use App\Customer;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function total_sale()
    {
        return $this->hasMany(Customer::class);
    }

    public function customers()
    {
    	return $this->hasMany(Customer::class);
    }

    public function reset()
    {
        return $this->hasMany(Reset::class);
    }
}
