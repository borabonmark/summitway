<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Session;
use Auth;
use Carbon\Carbon;

class PullOutResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'Store' => Auth::user()->store->name,
            'Barcode' => $this->itemlist->barcode,
            'Qty' => $this->qty,
            'Description' => $this->itemlist->desc,
            'Remarks' => $this->remarks,
            'Date' => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d H:i:s'),//Carbon::today()->toDateString(),
        ];
    }
}
