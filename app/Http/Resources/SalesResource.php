<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Session;
use Auth;
use App\TransactionReport;
use Carbon\Carbon; 
use App\Sale;
use App\Customer;
use App\PullOut;
use App\Order;
use App\OrderProduct;
use App\item_meta;
use App\Expense;

class SalesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $storeId = Session::get('store_id');
        $filterdate = Session::get('filter_date');
        $summary = [];
        $bonus_amount = 0;
        $desc = NULL;

        if($filterdate == Carbon::today()->toDateString())
        {
            $summary = $this->transactionSummary($storeId, $filterdate);
        }

        if(!empty($this->itemlist))
        {
            $bonus_amount = $this->itemlist->bonus_amount;
            $desc = $this->itemlist->desc;
        }

        $sales =  [
            'bonus' => strval($this->qty * $bonus_amount),
            'barcode' => $this->barcode,
            'description' => $desc,
            'price' => strval($this->price),
            'grandtotal' => strval($this->price*$this->qty),
            'qty' => strval($this->qty),
            'OS#' => strval(''),
            'OR#' => strval(''),
            'created_at' => date($this->created_at)
        ];

        return array_merge($sales, $summary);
    }

    private function transactionSummary($storeId, $filter_date)
    {
        
        $transactionReport = TransactionReport::where('date', $filter_date)->where('store_id', $storeId)->first();
        $customers = Customer::where('store_id', $storeId)->where('confirmed',true)->whereDate('created_at', $filter_date)->pluck('id')->toArray();
        $totalSold = Sale::whereIn('customer_id', $customers)->sum('qty');
        $pullOuts = PullOut::where('store_id', $storeId)->whereDate('created_at', $filter_date)->sum('qty');
       
        $order  = Order::where('store_id', $storeId)->whereDate('created_at', $filter_date)->pluck('id')->toArray();
        $delivered = OrderProduct::whereIn('order_id', $order)->sum('qty');

        $endingQty = item_meta::where('store_id', $storeId)->sum('qty');
        
       
        $summary = [
            'Beginning Qty' => $transactionReport->beginning_qty ?? 0,
            'Total Delivery' => $delivered,
            'Total Pull Out' => $pullOuts,
            'Total Sold' => $totalSold,
            'Ending Qty' => $endingQty,
        ];

        return $summary;
    }
}
