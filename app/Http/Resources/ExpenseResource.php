<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Session;
use Auth;
use Carbon\Carbon;
use App\Store;
class ExpenseResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $storeId = Session::get('store_id');
        $filterdate = Session::get('filter_date');

        $store = Store::findorfail($storeId);

        return  [
            // 'Store' => $store->name,
            'Expenses Amount' => $this->amount,
            'Expenses Details' => $this->details,
            'Expenses Transaction Time' => date($this->created_at)
        ];
    }
}
