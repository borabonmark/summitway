<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Sale extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'bonus' => strval($this->qty * $this->itemlist->bonus_amount),
            'barcode' => $this->barcode,
            'description' => $this->itemlist->desc,
            'price' => strval($this->price),
            'grandtotal' => strval($this->price*$this->qty),
            'created_at' => date($this->created_at),
            'qty' => strval($this->qty),
            'OS#' => strval(''),
            'OR#' => strval('')
        ];
    }
}
