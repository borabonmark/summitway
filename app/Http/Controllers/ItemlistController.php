<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Itemlist;
use App\Inventory_logs;
use App\SystemLog;
use Auth;

class ItemlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemlist= Itemlist::orderBy('id', 'DESC')->paginate(10);
        // dd($itemlist);
        return view('itemlist', compact('itemlist'));
    }


    public function getProductList(){
        $itemlist= Itemlist::orderBy('id', 'DESC')->paginate(50);

        return $itemlist;
    }

    public function store(Request $request)
    {
        $item = new Itemlist();
        $item->barcode=$request->get('barcode');
        $item->name=$request->get('name');
        $item->desc=$request->get('desc');
        $item->category=$request->get('category');
        $item->qty=$request->get('qty');
        $item->price=$request->get('price');

        if ($request->get('bonus')==1) {      
           $item->bonus_amount = $request->get('bonus_amount');
        }elseif($request->get('bonus')==2){
            $item->bonus_amount = $request->get('price')*0.05;
        }else{
            $item->bonus_amount = $request->get('bonus_amount');  
        }

        $item->bonus_type=$request->get('bonus');
        $item->save();

        $inventory_logs = new Inventory_logs();
        $inventory_logs->barcode = $request->get('barcode');
        $inventory_logs->action = 'addition';
        $inventory_logs->value = $request->get('qty');
        $inventory_logs->save();
        
        return redirect('itemlist')->with('success', 'Information has been added');
    }


    public function show($id)
    {
        $item = Itemlist::find($id);
        return view('itemlistUpdate', compact('item'));
    }

    public function update(Request $request, $id)
    {   
        //dd($id);
        //dd($request->all());
        $item = Itemlist::find($id);

        // system log
        SystemLog::saveLog(Auth::user()->store_id, Auth::user()->id, NULL, $item->id, $item->qty, $request->qty);

        $item->barcode = $request->get('barcode');
        $item->name = $request->get('name');
        $item->desc = $request->get('desc');
        $item->category = $request->get('category');
        $item->qty = $request->get('qty');
        $item->price = $request->get('price');
        if ($request->get('bonus')==1) {      
           $item->bonus_amount = $request->get('bonus_amount');
        }elseif($request->get('bonus')==2){
            $item->bonus_amount = $request->get('price')*0.05;
        }else{
            $item->bonus_amount = '';  
        }

        $item->bonus_type=$request->get('bonus');
        $item->save();

        $inventory_logs = new Inventory_logs();
        $inventory_logs->barcode = $item->barcode;
        $inventory_logs->action = 'addition';
        $inventory_logs->value = $request->get('qty');
        $inventory_logs->save();

    
        return redirect()->back()->with('success', 'Information has been save');
    }

    public function destroy($id)
    {
        $itemlist = Itemlist::find($id);
        $itemlist->delete();
        return redirect('itemlist')->with('success','Information has been  deleted');
    }

    public function updateqty(Request $request, $id)
    {   
        $item = Itemlist::find($id);
        $item->$request->get('qty');
        $item->save();
        return redirect('itemlist')->with('success', 'Information has been save');
    }

    public function getInventoryList()
    {
        $data['data'] = Itemlist::all();
        echo json_encode($data);
    }


    public function search(Request $request)
    {
        // dd($request->all());
        $itemlist  = Itemlist::where('barcode', $request->search)->orWhere('name', 'LIKE', '%' . $request->search . '%')->paginate(10);

        // dd($itemlist);
        return view('itemlist', compact('itemlist'));
    }
}
