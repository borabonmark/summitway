<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Customer;
use Auth;
use Carbon\Carbon;

// use Illuminate\Support\Facades\DB;


class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $stores =Store::with('reset')->get();

        //return $stores;
        return view('store',compact('stores'));
    }

    public function show(Store $store)
    {
        $from = Carbon::today();
        $to = Carbon::today()->addMinutes(1439)->addSeconds(59);
        $total_bonus = $store->customers->where('bonus_type', true)
                                       ->where('confirmed', true)
                                       ->where('created_at', '>=', $from)
                                       ->where('created_at', '<=', $to)
                                       ->sum('total');
      
        $dailySales = $store->customers->where('total_bonus', true)
                                    ->where('created_at', '>=', $from)
                                    ->where('created_at', '<=', $to)
                                    ->sortByDesc('created_at');
        $storeId = $store->id;
        return view('bonus', compact('total_bonus', 'dailySales', 'storeId'));
    }

    public function store(Request $request)
    {
        $store = new Store();
        $store->name=$request->get('name');
        $store->desc=$request->get('desc');
        $store->qouta=$request->get('qouta');
        $store->save();
        return redirect('store')->with('success', 'Information has been added');
    }

    public function update(Request $request, $store)
    {
        $store = Store::find($store->id);
        $store->name = $request->get('name');
        $store->desc = $request->get('desc');
        $store->qouta = $request->get('qouta');
        $store->closing_time = $request->closing_time;
        $store->save();
        return redirect('store')->with('success', 'Information has been saved');
    }

    public function destroy($store)
    {
        
        if($store->id==1){
            return redirect('store')->with('success','Sorry you cannot delete this details');    
        }else{
        $store = Store::find($store->id);
        $store->delete();
            return redirect('store')->with('success','Information has been  deleted');
        }
    }
}
