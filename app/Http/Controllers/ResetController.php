<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reset;


class ResetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reset = new Reset();
        $reset->store_id=$request->get('store_id');
        $reset->notes=$request->get('notes');
        $reset->status= true;
        $reset->save();
        return redirect()->back()->with('success', 'Reset Request has been submitted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        dd($request->all());

        $collections = item_meta::where('store_id', $request->store_id)->where('isTrue', true)->get();
        
        foreach($collections as $item)
        {
            $inventory = item_meta::find($item->id);
            $inventory->qty = 0;
            // $inventory->price = 0;
            $inventory->save(); 
        }

        $reset = Reset::find($id);
        $reset->status= false;
        $reset->save();
        return redirect()->back()->with('success', 'Reset Request has been verified!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
