<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Customer;
use Carbon\Carbon;
use App\Sale;
use App\Expense;
use App\Store;
use App\Http\Resources\Sale as SalesResource;
use DateTime;

class BonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $storeId = $request->storeId;

        $sales = Customer::where('store_id', $storeId)->whereDate('created_at', $request->filter_date)->sum('total');

        $total_expenses = Expense::where('store_id', $storeId)->whereDate('created_at', $request->filter_date)->sum('amount');
    

        $total_sales = $sales-$total_expenses;

        $qouta = Store::select('qouta')->where('id', $storeId)->get();


        $pin_bonus = Customer::where('store_id', $storeId)->where('confirmed', true)->where('bonus_type', true)->whereDate('created_at', $request->filter_date)->sum('total_bonus');
        $equipment_bonus = Customer::where('store_id', $storeId)->where('confirmed', true)->where('bonus_type', 2)->whereDate('created_at', $request->filter_date)->sum('total_bonus');
        $total_bonus = Customer::where('store_id', $storeId)->where('confirmed', true)->whereDate('created_at', $request->filter_date)->sum('total_bonus');
        $qouta_bonus = Customer::where('store_id', $storeId)->where('confirmed', true)->whereDate('created_at', $request->filter_date)->sum('total_bonus');

        // dd($qouta_bonus);    
        return view('bonus', compact('pin_bonus', 'equipment_bonus', 'total_bonus', 'qouta_bonus', 'sales', 'total_expenses', 'total_sales', 'qouta', 'storeId'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $sales = Customer::where('store_id', $id)->whereDate('created_at', Carbon::today())->sum('total');

        $total_expenses = Expense::where('store_id', $id)->whereDate('created_at', Carbon::today())->sum('amount');
    

        $total_sales = $sales-$total_expenses;

        $qouta = Store::select('qouta')->where('id', $id)->get();


        $pin_bonus = Customer::where('store_id', $id)->where('confirmed', true)->where('bonus_type', true)->whereDate('created_at', Carbon::today())->sum('total_bonus');
        $equipment_bonus = Customer::where('store_id', $id)->where('confirmed', true)->where('bonus_type', 2)->whereDate('created_at', Carbon::today())->sum('total_bonus');
        $total_bonus = Customer::where('store_id', $id)->where('confirmed', true)->whereDate('created_at', Carbon::today())->sum('total_bonus');
        $qouta_bonus = Customer::where('store_id', $id)->where('confirmed', true)->whereDate('created_at', Carbon::today())->sum('total_bonus');
        $storeId = $id;

        // dd($storeId);   
        return view('bonus', compact('pin_bonus', 'equipment_bonus', 'total_bonus', 'qouta_bonus', 'sales', 'total_expenses', 'total_sales', 'qouta', 'storeId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getJson($id)
    {
        $customers = Customer::where('store_id', $id)->where('confirmed',true)->whereDate('created_at', Carbon::today())->get()->pluck('id')->toArray();
        $data['data'] = SalesResource::collection(Sale::whereIn('customer_id', $customers)->get());
        echo json_encode($data);
    }


     public function getSalesDetails($id)
    {
        $customers = Customer::where('store_id', $id)->whereDate('created_at', Carbon::today())->get()->pluck('id')->toArray();
        $data['data'] = SalesResource::collection(Sale::whereIn('customer_id', $customers)->get());
        echo json_encode($data);
    }

}
