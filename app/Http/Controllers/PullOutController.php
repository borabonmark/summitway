<?php

namespace App\Http\Controllers;

use App\PullOut;
use Illuminate\Http\Request;
use App\Itemlist;
use App\item_meta;
use Auth;
use DB;

class PullOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pullOuts = PullOut::where('store_id', Auth::user()->store_id)->orderBy('id', 'desc')->get();
        
        if(Auth::user()->store_id === 1)
        {
            $pullOuts = PullOut::orderBy('id', 'desc')->get();
        }
        
        return view('pull-out', compact('pullOuts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validated = $request->validate([
            'barcode' => 'required',
            'store_id' => 'required',
            'qty' => 'required',
        ]);

        $product = Itemlist::where('barcode', $request->barcode)->first();

        // dd($product);
        if(empty($product)) return redirect()->back()->with('error', 'No Product Found!');


        $item_meta = item_meta::where('store_id', Auth::user()->store_id)->where('itemlist_id', $product->id)->first();
        
        if(empty($item_meta))
        {
            return  redirect()->back()->with('error', 'Product not found!');
        }

        if($item_meta->qty < $request->qty)
        {
            return  redirect()->back()->with('error', 'Your current inventory record is '. $item_meta->qty.'pcs only! less than the required pull out!');
        }

        $item = new PullOut();
        $item->store_id = $request->store_id;
        $item->itemlist_id = $product->id;
        $item->qty = $request->qty;
        $item->price = $request->price;
        $item->remarks = $request->remarks;
        $item->save();

        return redirect()->back()->with('success', 'Pull Out successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PullOut  $pullOut
     * @return \Illuminate\Http\Response
     */
    public function show(PullOut $pullOut)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PullOut  $pullOut
     * @return \Illuminate\Http\Response
     */
    public function edit(PullOut $pullOut)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PullOut  $pullOut
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PullOut $pullOut)
    {
 
        if(empty($pullOut)) return redirect()->back()->with('error', 'No Product Found!');

        $item =  PullOut::find($pullOut->id);
        $item->qty = $request->qty;
        $item->price = $request->price;
        $item->remarks = $request->remarks;
        $item->save();

        return redirect()->back()->with('success', 'Pull Out successfully added!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PullOut  $pullOut
     * @return \Illuminate\Http\Response
     */
    public function destroy(PullOut $pullOut)
    {
        $pullOut->delete();
        return redirect()->back()->with('success','Pull Out successfully  deleted');
    }

    public function approved(Request $request)
    {

        try
        {
            DB::beginTransaction();

            $pullOut = PullOut::find($request->pullout_id);
            $pullOut->is_approved = true;
            $pullOut->save();

    
            $item_meta = item_meta::where('store_id', $pullOut->store_id)->where('itemlist_id', $pullOut->itemlist_id)->first();
            $item_meta->qty = intval($item_meta->qty) - intval($pullOut->qty);
            $item_meta->save();

            

            $itemlist = Itemlist::find($pullOut->itemlist_id);
            $itemlist->qty = intval($itemlist->qty) + intval($pullOut->qty);
            $itemlist->save();

            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollback();
            return response()->json($e->getMessage(),500);
        }

        return redirect()->back()->with('success','Successfully updated!');
    }
}
