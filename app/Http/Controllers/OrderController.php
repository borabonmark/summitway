<?php

namespace App\Http\Controllers;

use Auth;
use App\Order;
use App\Store;
use App\OrderLog;
use Illuminate\Http\Request;
use App\Events\OrderStatusUpdated;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
                    
            if(Auth::user()->store_id==1){
                $orders = Order::orderBy("id", 'desc')->get();
                
                $stores = Store::where('id', '!=' ,Auth::user()->store_id)->get();
                return view('order', compact('orders', 'stores'));
            }else{
                $orders = Order::where('store_id', Auth::user()->store_id)->orderBy("id", 'desc')->get();
                return view('order', compact('orders'));
            }
        }else
        {
            return redirect('login');
        }
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $order = new Order();
        $order->store_id = $request->get('store_id');
        $order->status = 'pending';
        //$order->notes = 'test';
        $order->save();

        return redirect('orderproduct/'.$order->id)->with('success', 'Information has been saved');
    }

    public function show(Order $order)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->status = $request->get('status');
        $order->save();

        // Log status update
        $orderLog = OrderLog::create([
            'user_id' => Auth::id(),
            'order_id' => $order->id,
            'type' => OrderLog::TYPE_STATUS_UPDATE, 
            'message' => 'Order status has been updated to ' . $order->status
        ]);

        event(new OrderStatusUpdated($orderLog));

        return redirect('order')->with('success', 'New order created!');
    }


    public function destroy(Order $order)
    {
        
    }
}
