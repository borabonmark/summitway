<?php

namespace App\Http\Controllers;

use App\Inventory_logs;
use Illuminate\Http\Request;
use App\Itemlist;
use App\item_meta;

class InventoryLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inventories = NULL;
        return view('check-stocks', compact('inventories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barcode = $request->barcode;

        if(empty($barcode))
        {
            return redirect()->back()->with('error', 'No Barcode Found!');
        }

        $data = [];
        if($barcode)
        {
            $product = Itemlist::where('barcode', $barcode)->first();
            $inventories = item_meta::where('itemlist_id', $product->id)->get();
        }
        // dd($data);
        

        return view('check-stocks', compact('inventories', 'product'));

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory_logs  $inventory_logs
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if($request->barcode)
        {
            $inventories = Itemlist::where('barcode', $barcode)->get();
        }

        return view('check-stocks', compact('inventories'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory_logs  $inventory_logs
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory_logs $inventory_logs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory_logs  $inventory_logs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory_logs $inventory_logs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory_logs  $inventory_logs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventory_logs $inventory_logs)
    {
       
    }
}
