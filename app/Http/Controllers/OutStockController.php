<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OutStock;
use Auth;
use Illuminate\Support\Facades\DB;




class OutStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = DB::table('item_metas as i')
                    ->join('stores as s', 's.id', '=', 'i.store_id')
                    ->join('itemlists as l', 'l.id', '=', 'i.itemlist_id')
                    ->select('i.id','i.qty', 'l.desc', 'i.store_id', 'i.price', 's.id as sid', 's.name as storename', 'l.barcode')
                    ->where('i.store_id', $id)
                    ->where('i.qty', '<=', 100)
                    ->get();

        $storename = DB::table('stores')->select('name')->where('id',$id)->get();

        return view('outstock', compact('items', 'storename'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
