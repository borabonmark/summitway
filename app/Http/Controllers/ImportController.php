<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ItemImport;
use App\item_meta;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ImportController extends Controller 
{
    public function import(Request $request) 
    {
    	// dd($request->all());

		if ($request->get('store_id')) {

			item_meta::where('store_id', $request->get('store_id'))->delete();
	     	Excel::import(new ItemImport($request->store_id), request()->file('file'));
	        return redirect()->back()->with('success', 'Inventory Updated!');

		}else{
			return redirect()->back()->with('success', 'Please Specify Store!');
		}
    }
}