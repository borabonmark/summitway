<?php

namespace App\Http\Controllers;

use App\OrderProduct;
use App\Order;
use App\Itemlist;
use App\item_meta;
use App\Inventory_logs;
use Illuminate\Http\Request;
use Auth;
use DB;

class OrderProductController extends Controller
{

    public function index()
    {
         //$orderproducts=OrderProduct::where('store_id', Auth::user()->store_id)->where('order_id', 2)->get();
        //return view('orderproduct',compact('orderproducts'));
         return redirect('order')->with('success', 'Order does not exist');
    }

    public function create()
    {
        //
    }


    public function store(Itemlist $itemlist, Request $request)
    {

        // return $request->all();
        $validated = $request->validate([
            'barcode' => 'required',
            'order_id' => 'required',
            'qty' => 'required',
        ]);
        // return response()->json('Product not exist!');

        $itemlist=Itemlist::where('barcode', $request->barcode)->first();

        if(empty($itemlist))
        {
            return response()->json('error');
        }

        $order = Order::find($request->get('order_id'));

        if($itemlist->qty >= $request->get('qty'))
        {   

            $itemlist->qty = $itemlist->qty-$request->get('qty');
            $itemlist->save();

            $order->amount = $order->amount + ($itemlist->price*$request->get('qty'));
            $order->save();

            $order_item = new OrderProduct();
            $order_item->qty = $request->get('qty');
            $order_item->order_id = $request->get('order_id');
            $order_item->itemlist_id =  $itemlist->id;
            $order_item->qty = $request->get('qty');
           
            $order_item->price = $itemlist->price;
            $order_item->save();

            return redirect()->back()->with('success', 'Product inserted');
        }else
        {
            return redirect()->back()->with('success', 'Only ' . $itemlist->qty . ' remaining');
        }

    }

    public function show($id)
    {  
        $itemlist = Itemlist::all()->sortByDesc("id");

        $orderproducts=OrderProduct::where('order_id', $id)->orderBy('id', 'desc')->get();
        $order = Order::find($id);

        return view('orderproduct',compact('orderproducts' , 'itemlist', 'order', 'id'));
        

        if(Order::find($id))
        {
            // Order::where('store_id', Auth::user()->store_id)->firstOrFail()
            if(Auth::user()->store_id==1){
                $orderproducts=OrderProduct::where('order_id', $id)->get();
                return view('orderproduct',compact('orderproducts' , 'itemlist', 'order', 'id'));
            }
            else
            {
                $orderproducts=OrderProduct::where('order_id', $id)
                                        ->where('store_id', Auth::user()->store_id)->get();
                
                $count=$orderproducts->count();
                if($count>0){
                    return view('orderproduct',compact('orderproducts','order'));
                }
                else
                {
                    return redirect('order')->with('success', 'Order does not exist');
                }
            }
               
        }
        else
        {
            return redirect('order')->with('success', 'Order does not exist');
        }

    }

 
    public function update(Request $request, $id)
    {
        // dd($request->all());
       
    
        DB::beginTransaction();

        try {
            $order = Order::findOrFail($id);

            if($order)
            {
                $orderproducts = OrderProduct::where('order_id', $order->id)->get();

                foreach ($orderproducts as $orderproduct) 
                {
                    $item_exist = item_meta::where('itemlist_id', $orderproduct->itemlist_id)
                                            ->where('store_id', $order->store_id)->first();
                    
                    if ($item_exist) 
                    {        
                        $item_exist->qty += $orderproduct->qty;
                        $item_exist->save();

                        $inventory_logs = new Inventory_logs();
                        $inventory_logs->store_id = $order->store_id;
                        $inventory_logs->barcode = $item_exist->itemlist->barcode;
                        $inventory_logs->action = 'delivered';
                        $inventory_logs->value = $orderproduct->qty;
                        $inventory_logs->save(); 

                    }
                    else
                    {                    
                        $item_meta = new item_meta();
                        $item_meta->itemlist_id = $orderproduct->itemlist_id;
                        $item_meta->store_id = $order->store_id;
                        $item_meta->qty = $orderproduct->qty;
                        $item_meta->price = $orderproduct->price;
                        $item_meta->save();

                        $inventory_logs = new Inventory_logs();
                        $inventory_logs->store_id = $order->store_id;
                        $inventory_logs->barcode = $item_meta->itemlist->barcode;
                        $inventory_logs->action = 'delivered';
                        $inventory_logs->value = $orderproduct->qty;
                        $inventory_logs->save();
                        
                    }            
                }

                $order->notes =  $request->get('notes');
                $order->status = 'delivered';
                $order->save();
            }

            return redirect()->route('inventory.index')->with('success', 'Product has been delivered');   

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function destroy(Request $request, $id)
    {   
        $itemlist = Itemlist::find($request->get('item_id'));
        $itemlist->qty = $itemlist->qty+$request->get('qty');
        $itemlist->save();


        $order = Order::find($request->get('order_id')); 
        $order->amount = $order->amount-($request->get('qty')*$request->get('price')); 
        $order->save();

        $product = OrderProduct::find($id);
        $product->delete();
        return redirect('orderproduct/'.$request->get('order_id'))->with('success','Information has been  deleted');
    }

    public function getJsonOrder($id)
    {
        $data['data'] = OrderProduct::where('order_id', $id)->get();
        echo json_encode($data);
    }
}
