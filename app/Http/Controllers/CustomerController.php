<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Sale;
use App\item_meta;
use App\Itemlist;
use Illuminate\Http\Request;
use Auth;
use App\SystemLog;


class CustomerController extends Controller
{

    public function index()
    {
        // // $items=Customer::where('store_id', Auth::user()->store_id)->get();
        // $items=sale::all();
        // return view('pos', compact('items'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        // dd('dsa');

        // Customer::deleteEmptyChild();

        $id = $request->id;
        if($id != null)
        {   
            $sales = Customer::find($id);
            $sales->confirmed = 1;
            $sales->save();

            // SystemLog::saveLog($sales->store_id, $sales->user_id, $sales->id);


            $sold = Sale::where('customer_id', $id)->get();

            if($sold)
            {  
                foreach($sold as $item)
                {
                    $itemlist=itemlist::where('barcode', $item->barcode)->firstOrFail();
                    $item_meta=item_meta::where('itemlist_id', $itemlist->id)
                            ->where('store_id', Auth::user()->store_id)->first(); 
                    if( $item_meta->qty > 0 ){
                       
                        $item_meta->qty = $item_meta->qty-$item->qty;
                        $item_meta->save();
                    } 
                    
                    // SystemLog::saveLog($invoice->store_id, $invoice->user_id, $invoice->id); // Logs
                    SystemLog::saveLog(Auth::user()->store_id, Auth::user()->id, $item_meta->id, $item_meta->itemlist_id, $item_meta->qty, $item->qty);

                }
            }
        } else {

            $invoice = new Customer();
            $invoice->store_id = Auth::user()->store_id;
            $invoice->user_id = Auth::user()->id;
            $invoice->save();

            if($invoice)
            {
                // SystemLog::saveLog($invoice->store_id, $invoice->user_id, $invoice->id); // Logs
                SystemLog::saveLog(Auth::user()->store_id, Auth::user()->id, $invoice->id, $invoice->itemlist_id, NULL, $invoice->qty);

                $id = $invoice->id;
            }

        }
        
        return redirect('pos/'.$id)->with('success', 'Transaction completed');      
    }




    public function show($id)
    {   
        
        
        // $items=sale::all();
        // return view('pos', compact('items'));
    }


    public function edit(Customer $customer)
    {
        //
    }


    public function update(Request $request, Customer $customer)
    {
        //
    }


    public function destroy(Customer $customer)
    {
        //
    }
}
















