<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Customer;
use App\item_meta;
use App\Itemlist;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SaleController extends Controller
{


    public function index()
    {

        $sales= Customer::where('store_id', Auth::user()->store_id)->where('confirmed', true)->whereDate('created_at', Carbon::today())->orderBy('created_at', 'desc')->get();
        $grand_total = DB::table('customers')->where('store_id', Auth::user()->store_id)->whereDate('created_at', Carbon::today())->where('confirmed', true)->sum('total');
        $total_bonus  = DB::table('customers')->where('store_id', Auth::user()->store_id)->whereDate('created_at', Carbon::today())->where('confirmed', true)->sum('total_bonus');
        return view('pos2', compact('sales', 'total_bonus', 'grand_total'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        
        // return $request->all();

        $itemlist = itemlist::where('barcode', $request->get('barcode'))->firstOrFail();

        

        $item_meta=item_meta::where('itemlist_id', $itemlist->id)
                    ->where('store_id', Auth::user()->store_id)->first();   

        // return $item_meta->qty . " - " .$request->qty;
        if($request->qty > $item_meta->qty)
        {
            return response()->json(['error'=>'The remaining Qty is '. $item_meta->qty. ' ONLY!']);
        }
        
        if ($item_meta->qty <= 0) {
            # code...
            return response()->json(['error'=>'Out of Stock!']);
        }

        if($item_meta == null)
        {
            return response()->json(['error'=>'Item Not Found!']);           
        }
        else
        {
            $item = new Sale();
            $item->barcode = $request->get('barcode');
            $item->customer_id = $request->get('customer_id');

            if ($request->get('price') == null) {
                $item->price = $itemlist->price;
            }else{
                $item->price = $request->get('price');
            }
            
            $item->qty = $request->get('qty');
            $item->save();

            $invoice = Customer::find($request->get('customer_id'));
            
            if ($request->get('price') == '') {
                $invoice->total = $invoice->total+($itemlist->price*$request->get('qty'));
            }else{
                $invoice->total = $invoice->total+($request->get('price')*$request->get('qty'));
            }

            if ($itemlist->bonus_type === 2) {
               $invoice->total_bonus = $invoice->total_bonus+($request->get('price')*0.05);
            }else{
                $invoice->total_bonus = $invoice->total_bonus+($itemlist->bonus_amount*$request->get('qty'));
            }

            $invoice->bonus_type = $itemlist->bonus_type;
            
            $invoice->save();

                   
            return $invoice->total;
        }
    }


    public function show($id)
    {

        // dd($id);
        $items = Sale::where('customer_id', $id)->get();
        $invoice = Customer::find($id);
        
        return view('pos', compact('items', 'invoice'));
    }


    public function edit(Sale $sale)
    {
        //
    }


    public function update(Request $request, Sale $sale)
    {
        //
    }


    public function destroy(Sale $sale)
    {
        //
    }

   
}
