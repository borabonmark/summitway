<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use Auth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }


    public function login(Request $request)
    {   

        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',            
        ]);
        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'activate'=>'1']))
        {      
            return redirect('/inventory')->with('SuccessLogin', $request->email);
        }else
        {
           return redirect('/signin')->with('Status', 'Email or password might be incorrect or account is not yet verified');
        }
    
        // return redirect('/')->with('Status', 'You are registered');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
}
