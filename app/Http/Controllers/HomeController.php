<?php

namespace App\Http\Controllers;

use App\Order;
use App\TransactionReport;
use App\Customer;
use App\item_meta;
use App\Store;
use App\SystemLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param \App\Store
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        SystemLog::saveLog(Auth::user()->store_id, Auth::user()->id); // System Log
        $this->saveBeginningQty(); // saving beginning balance

        $orders = [];
        $currentMonth = Carbon::now()->month;
        $firstDaTeTimeOfTheYear = Carbon::now()->startOfMonth()->subMonth($currentMonth-1); 
        $firstDateTimeOfTheMonth = Carbon::now()->startOfMonth(); 
        $lastDateTimeOfTheMonth = Carbon::now()->endOfMonth(); 
        
        $totalFor = 'Sales';
        $amountTotalFor = [];
        $perMonthAmountTotal = [];
        $perDayAmountTotal = [];

        // if admin and the submitted store is warehouse
        if ((Auth::user()->id == 1) && !empty($store->id) && (Auth::user()->store->id == $store->id)) {
            $totalFor = 'Orders';
        }

        // if admin and no store
        if ((Auth::user()->id == 1) && empty($store->id)) {
            $totalFor = 'Orders';
        }

        // if not admin, automatic set the store to it's own store.
        if (Auth::user()->id != 1) {
            $store = Auth::user()->store;
        }

        if ('Sales' == $totalFor) {
            $sales = DB::table('customers')
                     ->select(DB::raw('total, confirmed,  created_at, MONTH(created_at) as month, DAY(created_at) as day'))
                     ->whereBetween('created_at', [$firstDaTeTimeOfTheYear, $lastDateTimeOfTheMonth])
                     ->where('confirmed', '1')
                     ->where('store_id', $store->id)
                     ->get();

            list($monthlyAmountTotal, $dailyAmountTotal) = $this->getAmountTotal($sales, 'total');
            $amountTotalFor[] = 'Sales';
            $perMonthAmountTotal[] = $monthlyAmountTotal;
            $perDayAmountTotal[] = $dailyAmountTotal;
            return view('dashboard', compact('amountTotalFor', 'perMonthAmountTotal', 'perDayAmountTotal', 'store'));
        }

        $sales = DB::table('customers')
                   ->select(DB::raw('total, confirmed,  created_at, MONTH(created_at) as month, DAY(created_at) as day'))
                   ->whereBetween('created_at', [$firstDaTeTimeOfTheYear, $lastDateTimeOfTheMonth])
                   ->where('confirmed', '1')
                   ->get();

        list($monthlyAmountTotal, $dailyAmountTotal) = $this->getAmountTotal($sales, 'total');
        $amountTotalFor[] = 'Sales';
        $perMonthAmountTotal[] = $monthlyAmountTotal;
        $perDayAmountTotal[] = $dailyAmountTotal;

        $orders = DB::table('orders')
             ->select(DB::raw('amount, status,  created_at, MONTH(created_at) as month, DAY(created_at) as day'))
             ->whereBetween('created_at', [$firstDaTeTimeOfTheYear, $lastDateTimeOfTheMonth])
             ->where('status', 'delivered')
             ->get();

        list($monthlyAmountTotal, $dailyAmountTotal) = $this->getAmountTotal($orders, 'amount');
        $amountTotalFor[] = 'Orders';
        $perMonthAmountTotal[] = $monthlyAmountTotal;
        $perDayAmountTotal[] = $dailyAmountTotal;

        return view('dashboard', compact('amountTotalFor', 'perMonthAmountTotal', 'perDayAmountTotal', 'store'));
    }


    public function saveBeginningQty()
    {
        
        $storeId = Auth::user()->store_id;

        if($storeId != 1)
        {
            $hasBeginningQty = TransactionReport::where('store_id', $storeId)->whereDate('created_at', Carbon::today())->first();

           if(empty($hasBeginningQty))
           {
                $beginning = item_meta::where('store_id', $storeId)->sum('qty');

                $transactionReport = new TransactionReport();
                $transactionReport->store_id = $storeId;
                $transactionReport->beginning_qty = $beginning;
                $transactionReport->date = Carbon::today()->toDateString();
                $transactionReport->save();
           }
        }
    }

    public function getAmountTotal($data, $column = 'amount')
    {
        $currentMonth = Carbon::now()->month;
        $firstDaTeTimeOfTheYear = Carbon::now()->startOfMonth()->subMonth($currentMonth-1); 
        $firstDateTimeOfTheMonth = Carbon::now()->startOfMonth(); 
        $lastDateTimeOfTheMonth = Carbon::now()->endOfMonth(); 

        /**
         * Group orders or sales  per 
         * month or day of current month
         */
        $perMonth = $data->groupBy('month');
        $perMonthAmountTotal = [];

        // Get only the months from january to current month.
        for ($month = 1; $month <= $currentMonth; $month++) {
            $perMonthAmountTotal['months'][$month] =  date('F', mktime(0, 0, 0, $month, 1, date('Y')));
            $perMonthAmountTotal['amount_total'][$month] = 0;
        }

        // Set orders or sales per month amount total
        foreach ($perMonth as $month => $ordersOrSales) {
            $perMonthAmountTotal['amount_total'][$month] = $ordersOrSales->sum($column);
        }

        /**
         * Group orders per day of
         * this current month
         */
        $perDay = $data->where('month', $currentMonth)->groupBy('day');
        $perDayAmountTotal = [];

        // Get only the days for current month.
        for ($day = 1; $day <= $lastDateTimeOfTheMonth->day; $day++) {
            $perDayAmountTotal['days'][$day] =  date('F d', mktime(0, 0, 0, $currentMonth, $day, date('Y')));
            $perDayAmountTotal['amount_total'][$day] = 0;
        }

        // Set orders or sales per day amount total
        foreach ($perDay as $day => $ordersOrSales) {
            $perDayAmountTotal['amount_total'][$day] = $ordersOrSales->sum($column);
        }

        return [$perMonthAmountTotal, $perDayAmountTotal];
    }
}
