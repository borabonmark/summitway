<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Export\ExcelExport;
use App\Export\ExportPullOut;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Export\ExportExpense;
use App\Store;

class ExcelExportController extends Controller
{

    public function export(Request $request) 
    {
        $store = Store::findorfail($request->store_id);
        return Excel::download(new ExcelExport($request->filter_date, $request->store_id), ucfirst($store->name).'_Daily_Sales_'.Carbon::today().'.xlsx');
    }

    public function exportPullOut($id) 
    {
        return Excel::download(new ExportPullOut($id), 'PullOuts_'.Carbon::today().'.xlsx');
    }

    public function exportExpense(Request $request) 
    {
        $store = Store::findorfail($request->store_id);
        return Excel::download(new ExportExpense($request->filter_date, $request->store_id), ucfirst($store->name).'_Daily_Expenses_'.Carbon::today().'.xlsx');
    }
}
