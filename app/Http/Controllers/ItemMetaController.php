<?php

namespace App\Http\Controllers;

use App\item_meta;
use App\Itemlist;
use Illuminate\Http\Request;
use Auth;
class ItemMetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderproducts=item_meta::where('store_id', Auth::user()->store_id)->where('isTrue', true)->get();
        $items=Itemlist::all();

        //dd($orderproducts);

        //return $orderproducts;
        return view('inventory',compact('orderproducts', 'items'));
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $check=item_meta::where('itemlist_id', $request->get('item'))
                        ->where('store_id', Auth::user()->store_id)->get();
        if ($check->count()==0) 
        {           
            $item = new item_meta();
            $item->qty = $request->get('qty');
            $item->itemlist_id = $request->get('item');
            $item->store_id = Auth::user()->store_id;
            $item->qty = $request->get('qty');
            $item->save();
            return redirect('inventory')->with('success', 'Information has been saved');
        }else{
            return redirect('inventory')->with('success', 'Item already exist');
        }

    }

    
    public function show(item_meta $item_meta)
    {
        //
    }


    public function update(Request $request, $id)
    {

        print_r('goes hrere');
        exit();
        $item = item_meta::find($id);
        // $item->qty = $item->qty+$request->get('qty');
        //$item->price = $request->get('price');
        $item->save();
        return redirect('inventory')->with('success', 'Information has been saved');
    }


    public function destroy(item_meta $item_meta)
    {
        //
    }
}
