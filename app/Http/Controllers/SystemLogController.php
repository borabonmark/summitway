<?php

namespace App\Http\Controllers;

use App\SystemLog;
use Illuminate\Http\Request;
use Auth;
use App\Store;
use App\Customer;
class SystemLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        $logs = SystemLog::where('store_id', Auth::user()->store_id)->orderBy('id', 'desc')->get();
        return view('system-logs', compact('logs', 'stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $stores = Store::all();
        $store = Store::find($request->store_id);

        
        if($store)
        {
            $logs = SystemLog::where('store_id',  $store->id)->orderBy('id', 'desc')->get();
            
        } else {
            $logs = SystemLog::where('store_id', Auth::user()->store_id)->orderBy('id', 'desc')->get();
            
        }
        
        return view('system-logs', compact('logs', 'stores', 'store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SystemLog  $systemLog
     * @return \Illuminate\Http\Response
     */
    public function show(SystemLog $systemLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SystemLog  $systemLog
     * @return \Illuminate\Http\Response
     */
    public function edit(SystemLog $systemLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SystemLog  $systemLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SystemLog $systemLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SystemLog  $systemLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(SystemLog $systemLog)
    {
        //
    }
}
