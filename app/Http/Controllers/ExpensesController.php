<?php

namespace App\Http\Controllers;

use App\Expense;
use Illuminate\Http\Request;
use Auth;
class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //dd(date('m'));
        $expenses = Expense::where('store_id', Auth::user()->store_id)->get();
        $totalExpenses = Expense::where('store_id', Auth::user()->store_id)->whereMonth('created_at', '=', date('m'))->sum('amount');
        return view('expenses', compact('expenses', 'totalExpenses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Expense();
        $data->amount=$request->get('amount'); 
        $data->store_id = Auth::user()->store_id;
        $data->details=$request->get('details');
        $data->save();
        return redirect('expenses')->with('success', 'Data has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  Expense::find($id);
        $data->amount=$request->get('amount'); 
        $data->details=$request->get('details');
        $data->save();
        return redirect('expenses')->with('success', 'Data has been added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $user = Expense::find($id);
            $user->delete();
            return redirect('expenses')->with('success','Information has been  deleted');
    }
}
