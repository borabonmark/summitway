<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Store;
use Auth;

class RegistrationController extends Controller
{

    public function index()
    {
        if(Auth::user()->id==1){
            $users=User::all();
            // $users=User::where('users.store_id', '=', 1)->get();
            $stores=Store::all();
            return view('user',compact('stores', 'users'));
        }else{
            $users = User::where('store_id', Auth::user()->store_id)->get();
            $stores=Store::all();
            return view('user',compact('users', 'stores'));
        }
        
        
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->store_id = $request->input('store_id');
        $user->type = $request->get('type');
        $user->password=Hash::make($request->get('password'));
        $user->save();
        return redirect('user')->with('success', 'Information has been added');
    }

   
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->store_id = $request->get('store_id');
        $user->type = $request->get('type');
        $user->password = Hash::make($request->get('password'));
        $user->save();
        return redirect('user')->with('success', 'Information has been saved');
    }

    public function destroy($id)
    {
        if($id==1){
            return redirect('user')->with('success','Sorry you cannot delete this details');    
        }else{
            $user = User::find($id);
            $user->delete();
            return redirect('user')->with('success','Information has been  deleted');
        }
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'store_id' => 'required',
            'type' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
}
