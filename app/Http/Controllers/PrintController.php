<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Customer;
use Carbon\Carbon;
use App\Sale;
use App\Expense;
use App\Store;
use App\Reference\Bonus;

class PrintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = Store::find($id);

        $sales = Customer::where('confirmed', true)->where('store_id', $id)->whereDate('created_at', Carbon::today())->sum('total');

        $total_expenses = Expense::where('store_id', $id)->whereDate('created_at', Carbon::today())->sum('amount');
        $expenses = Expense::where('store_id', $id)->whereDate('created_at', Carbon::today())->get();
    


        
        $total_sales = $sales-$total_expenses;

        $pin_bonus = Customer::where('store_id', $id)->where('confirmed', true)->where('bonus_type', 1)->whereDate('created_at', Carbon::today())->sum('total_bonus');
        $equipment_bonus = Customer::where('store_id', $id)->where('confirmed', true)->where('bonus_type', Bonus::EQUIPMENT)->whereDate('created_at', Carbon::today())->sum('total_bonus');
        $qouta_bonus = Customer::where('store_id', $id)->where('confirmed', true)->where('bonus_type', Bonus::PIN_MONEY)->where('confirmed', true)->whereDate('created_at', Carbon::today())->sum('total');

        return view('print-out', compact('store', 'expenses','pin_bonus', 'equipment_bonus', 'qouta_bonus', 'sales', 'total_expenses', 'total_sales'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
