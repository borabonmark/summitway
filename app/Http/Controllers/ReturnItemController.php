<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Return_item;
use Auth;
use App\Itemlist;
use App\item_meta;
class ReturnItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //return view('returnItem');
        if(Auth::user()->store_id===1 && Auth::user()->type===1) {
            $returns = Return_item::all();
        }else {
            $returns =  Return_item::where('store_id', Auth::user()->store_id)->get();
        }


        

        return view('returnItem',compact('returns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());

        $product = Itemlist::where('barcode', $request->barcode)->first();

        if(empty($product)) return redirect()->back()->with('error', 'No Product Found!');

        $storeItem = item_meta::where('itemlist_id', $product->id)->where('store_id', $request->store_id)->first();
        
        if(empty($storeItem)) return redirect()->back()->with('error', 'No Product Found!');

        $item = new Return_item();
        $item->store_id = $request->store_id;
        $item->barcode = $request->barcode;
        $item->qty = $request->qty;
        $item->price = $request->price;
        $item->notes = $request->notes;
        $item->save();

        return redirect('return')->with('success', 'Data has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = Return_item::find($id);
        $item->barcode = $request->get('barcode');
        $item->store_id=Auth::user()->store_id;
        $item->name = $request->get('name');
        $item->qty = $request->get('qty');
        $item->price = $item->price+$request->get('price');
        $item->save();
        return redirect('return')->with('success', 'Information has been save');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $return = Return_item::find($id);
        $return->delete();
        return redirect('return')->with('success','Information has been  deleted');
    }

    public function approved(Request $request)
    {
        $return = Return_item::find($request->return_id);
        $return->is_approved = true;
        $return->save();

        $product = Itemlist::where('barcode', $return->barcode)->first();

        $inventory = item_meta::where('itemlist_id', $product->id)->where('store_id', $return->store_id)->first();
        $inventory->qty = intval($inventory->qty) + intval($return->qty);
        $inventory->save();

    
        return redirect('return')->with('success','Successfully updated!');

    }
}
