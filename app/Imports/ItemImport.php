<?php

namespace App\Imports;

use App\item_meta;
use App\Itemlist;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ItemImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    private $store_id;

    public function __construct($store_id)
    {
        $this->store_id = $store_id;
    }

    public function model(array $row)
    {

        $product = Itemlist::where('barcode', $row['barcode'])->first();
        
        if($product)
        {
            $item = item_meta::where('store_id', $this->store_id)->where('itemlist_id', $product->id)->first();

            if(!empty($item))
            {
                $item->qty = $row['quantity'];
                $item->save();
                return $item;
            }else{
                return new item_meta([
                    'store_id'     => $this->store_id,
                    'itemlist_id' => $product->id,
                    'qty'    => $row['quantity'], 
                ]);
            }
        }  
    }
}
