<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemLog extends Model
{
    public static function saveLog($storeId = NULL, $userId = NULL, $customerId = NULL, $itemlist_id = NULL, $old_qty = NULL, $qty = NULL)
    {
        $log = new self();
        $log->store_id = $storeId;
        $log->user_id = $userId;
        $log->customer_id = $customerId;
        $log->itemlist_id = $itemlist_id;
        $log->old_qty = $old_qty;
        $log->qty = $qty;
        $log->save();
    }

    public function transactions()
    {
        return $this->belongsTo(Customer::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function itemlist()
    {
        return $this->belongsTo(Itemlist::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
