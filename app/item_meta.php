<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class item_meta extends Model
{
	protected $fillable = [
        'store_id', 'itemlist_id', 'qty', 'price', 
    ];

    public function itemlist()
    {
        return $this->belongsTo(Itemlist::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
