<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function itemlist()
    {
        return $this->belongsTo(Itemlist::class, 'barcode', 'barcode');
    }

    public function item_meta()
    {
        return $this->hasMany(item_meta::class, 'itemmeta_id');
    }
}

