<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware' => 'auth'], function(){
	Route::resource('reset', 'ResetController');
	Route::resource('print', 'PrintController');
	Route::get('get-inventory', 'ItemlistController@getInventoryList')->name('get-inventory');
	Route::post('get-product-list', 'ItemlistController@getProductList');
	Route::post('/search', 'ItemlistController@search');
	Route::resource('itemlist', 'ItemlistController');
	Route::resource('store', 'StoreController');
	Route::resource('user', 'RegistrationController');
	Route::get('get-json-order/{order}', 'OrderProductController@getJsonOrder')->name('get-json-order');
	Route::resource('order', 'OrderController');
	Route::resource('orderproduct', 'OrderProductController');
	Route::resource('inventory', 'ItemMetaController');
	Route::resource('pos', 'SaleController');
	Route::resource('invoice', 'CustomerController');

	Route::put('return', [
        'as' => 'return.approved',
        'uses' => 'ReturnItemController@approved'
    ]);

	Route::put('pull-outs', [
        'as' => 'pull-outs.approved',
        'uses' => 'PullOutController@approved'
    ]);
	
	Route::get('pull-outs/export/{id}', [
        'as' => 'pull-outs.export',
        'uses' => 'ExcelExportController@exportPullOut'
    ]);


	Route::resource('return', 'ReturnItemController');
	Route::resource('pull-outs', 'PullOutController');
	Route::resource('outstock', 'OutStockController');
	Route::resource('bonus', 'BonusController');
	Route::get('/get-json/{id}', 'BonusController@getJson')->name('get-json');
	Route::get('/get-sales/{id}', 'BonusController@getSalesDetails'); // test
	Route::resource('export', 'ExportController');
	Route::resource('expenses', 'ExpensesController');

	Route::resource('transactions', 'SystemLogController');

	Route::resource('inventories', 'InventoryLogsController');

	Route::post('/import', 'ImportController@import');

	
	Route::post('/export-daily-sales', 'ExcelExportController@export');
	Route::post('/export-daily-expenses', 'ExcelExportController@exportExpense');

	Route::get('/', 'HomeController@index')->name('dashboard');
	Route::get('/home', 'HomeController@index')->name('dashboard');
	Route::get('/dashboard/{store}', 'HomeController@index')->name('dashboard');
});

Auth::routes();
//Route::get('/', 'HomeController@index')->name('dashboard');




