<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePullOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pull_outs', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('store_id');
            $table->integer('itemlist_id');
            $table->boolean('is_approved')->default(false);
            $table->bigInteger('qty')->nullable();
            $table->double('price')->nullable();
            $table->mediumText('remarks')->nullable();
            $table->string('date_received')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pull_outs');
    }
}
