<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itemlists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barcode', 100)->unique();
            $table->string('name', 100)->nullable();
            $table->string('desc', 100)->nullable();
            $table->string('category', 100)->nullable();
            $table->bigInteger('qty')->default(1);
            $table->double('price')->default(1);
            $table->tinyInteger('bonus_type')->nullable(1);
            $table->double('bonus_amount')->nullable(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itemlists');
    }
}
