<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
       $users=[
            [
                'name' => 'Warehouse',
                'email' => 'warehouse@gmail.com',
                'password' => bcrypt('secret'),
                'store_id' => 1,
                'type' => 1,
            ],
            [
                'name' => 'Norman',
                'email' => 'norman.warehouse@gmail.com',
                'password' => bcrypt('secret'),
                'store_id' => 1,
                'type' => 2,
            ],
            [
                'name' => 'Angeles Branch',
                'email' => 'angeles@gmail.com',
                'password' => bcrypt('secret'),
                'store_id' => 2,
                'type' => 1,
            ],
            [
                'name' => 'Mabalacat Branch',
                'email' => 'mabalacat@gmail.com',
                'password' => bcrypt('secret'),
                'store_id' => 3,
                'type' => 1,
            ],
            [
                'name' => 'Bryan',
                'email' => 'bryan.mabalacat@gmail.com',
                'password' => bcrypt('secret'),
                'store_id' => 2,
                'type' => 2,
            ],
            [
                'name' => 'Kiel',
                'email' =>'keil.warehouse@gmail.com',
                'password' => bcrypt('secret'),
                'store_id' => 1,
                'type' => 2,
            ]
        ];

        DB::table('users')->insert($users);
            

        $stores=[
            [
            'name' => 'Warehouse',
            'desc' => 'This the warehouse',
            'qouta' => '0',
            ],
            [
            'name' => 'Angeles Branch',
            'desc' => 'Unit 17 Nepo Mall',
            'qouta' => '20000',
            ],
            [
            'name' => 'Mabalacat Branch',
            'desc' => 'Unit 67 Marina Arcade Dau Mabalacat',
            'qouta' => '15000',
            ]
        ];

        DB::table('stores')->insert($stores);


        $itemlists=[
            [
            'barcode' => str_random(8),
            'name' => 'Martilyo',
            'desc' => 'Initial item 1',
            'category' => '1',
            'qty' => 3230,
            'price' => 50,
            'bonus_type' => 1,
            'bonus_amount' => 10,
            ],
            [
            'barcode' => str_random(8),
            'name' => 'Pako',
            'desc' => 'Initial item 2',
            'category' => '1',
            'qty' => 1230,
            'price' => 25,
            'bonus_type' => 1,
            'bonus_amount' => 10,
            ],
            [
            'barcode' => str_random(8),
            'name' => 'Lagare',
            'desc' => 'Initial item',
            'category' => '2',
            'qty' => 4430,
            'price' => 15,
            'bonus_type' => 1,
            'bonus_amount' => 10,
            ]
        ];

        DB::table('itemlists')->insert($itemlists);

        /*$itemmeta=[
            [
            'store_id' => 2,
            'itemlist_id' => 1,
            'qty' => 30,
            'price' => 50,
            ],
            [
            'store_id' => 2,
            'itemlist_id' => 2,
            'qty' => 30,
            'price' => 50,
            
            ],
            [
            'store_id' => 2,
            'itemlist_id' => 3,
            'qty' => 30,
            'price' => 50,
            
            ],
            [
            'store_id' => 3,
            'itemlist_id' => 3,
            'qty' => 30,
            'price' => 50,
            
            ],
            [
            'store_id' => 3,
            'itemlist_id' => 1,
            'qty' => 20,
            'price' => 10,
            
            ]

        ];

        DB::table('item_metas')->insert($itemmeta);

        $orders=[
            [
            'store_id' => 2,
            'user_id' => 1,
            'amount' => 50,
            'received_by' => 2,
            'notes'=>'test',
            ],
            [
            'store_id' => 2,
            'user_id' => 1,
            'amount' => 50,
            'received_by' => 2,
            'notes'=>'test',
            ],
            [
            'store_id' => 3,
            'user_id' => 3,
            'amount' => 50,
            'received_by' => 3,
            'notes'=>'test'
            ]
        ];

        DB::table('orders')->insert($orders);

        $orderproducts=[
            [
            'order_id' => 1,
            'itemlist_id' => 2,
            'qty' => 30,
            'price' => 50,

            ],
            [
            'order_id' => 1,
            'itemlist_id' => 1,
            'qty' => 30,
            'price' => 50,           
            ],
            [
            'order_id' => 2,
            'itemlist_id' => 1,
            'qty' => 30,
            'price' => 50,           
            ],
            [
            'order_id' => 2,
            'itemlist_id' => 3,
            'qty' => 30,
            'price' => 50,            
            ]
        ];

        DB::table('order_products')->insert($orderproducts); */
    }
}
