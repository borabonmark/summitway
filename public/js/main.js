// BETTER
var PI = {
 
    onReady: function() {
    	$body = $('body');
        $body.on('change', 'input[name="1k"]', PI.compute1k);
        $body.on('change', 'input[name="500"]', PI.compute500);
        $body.on('change', 'input[name="100"]', PI.compute100);
        $body.on('change', 'input[name="50"]', PI.compute50);
        $body.on('change', 'input[name="20"]', PI.compute20);
        $body.on('change', 'input[name="10"]', PI.compute10);
        $body.on('change', 'input[name="5"]', PI.compute5);
        $body.on('change', 'input[name="1"]', PI.compute1);
        PI.checkRadio();
        
    },
 	compute1k: function(){
 		var value = $('input[name="1k"]').val(), total = 1000 * value;
 		$('#1k_total').val(total);
 		PI.computeAll(total);
        PI.computeShortCash();
        $('input[name="1k"]').prop('disabled', true);
 		return total;
 	},
 	compute500: function(){
 		var value = $('input[name="500"]').val(), total = 500 * value;
 		$('#500_total').val(total);
 		PI.computeAll(total);
        PI.computeShortCash();
        $('input[name="500"]').prop('disabled', true);
 		return total;
 	},
 	compute100: function(){
 		var value = $('input[name="100"]').val(), total = 100 * value;
 		$('#100_total').val(total);
        PI.computeAll(total);
        PI.computeShortCash();
        $('input[name="100"]').prop('disabled', true);
 		return total;
 	},
 	compute50: function(){
 		var value = $('input[name="50"]').val(), total = 50 * value;
 		$('#50_total').val(total);
        PI.computeAll(total);
        PI.computeShortCash();
        $('input[name="50"]').prop('disabled', true);
 		return total;
 	},
 	compute20: function(){
 		var value = $('input[name="20"]').val(), total = 20 * value;
 		$('#20_total').val(total);
        PI.computeAll(total);
        PI.computeShortCash();
        $('input[name="20"]').prop('disabled', true);
 		return total;
 	},
 	compute10: function(){
 		var value = $('input[name="10"]').val(), total = 10 * value;
 		$('#10_total').val(total);
        PI.computeAll(total);
        PI.computeShortCash();
        $('input[name="10"]').prop('disabled', true);
 		return total;
 	},
 	compute5: function(){
 		var value = $('input[name="5"]').val(), total = 5 * value;
 		$('#5_total').val(total);
        PI.computeAll(total);
        PI.computeShortCash();
        $('input[name="5"]').prop('disabled', true);
 		return total;
 	},
 	compute1: function(){
 		var value = $('input[name="1"]').val(), total = 1 * value;
 		$('#1_total').val(total);
        PI.computeAll(total);
        PI.computeShortCash();
        $('input[name="1"]').prop('disabled', true);
 		return total;
 	},
 	computeAll: function(e){
 		var sealed = $('#sealed').val();
 		var r  = +sealed+ +e;
        $('#sealed').val(r);
 		//console.log(sealed);
 		//console.log(r);
 	},
    computeShortCash: function(){
        console.log($('#grand-total').val());
        var grandTotal = parseInt($('#grand-total').val()), sealed = parseInt($('#sealed').val())
        if (grandTotal > sealed) {
            $('#over-in-cash').val('');
            $('#short-in-cash').val(grandTotal - sealed);
        }else{
            $('#short-in-cash').val('');
            $('#over-in-cash').val(sealed- grandTotal);
        }
    },
    checkRadio: function(){
        $("input[type=radio]").click(function () {
            if ($(this).val() == 2) {
                $('input[name="bonus_amount"]').val('0').hide();
            }else{
                $('input[name="bonus_amount"]').show().prop('required',true);
            }
            
        });
    },


};
 
$( document ).ready( PI.onReady );